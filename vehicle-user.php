<form id="vehicle_info_form" class="page-form" method="post" action="#">

    <div class="form-group form-fields clearfix">
        <p>
            <label><?php _e('Full name', 'ns');?> (*)</label>
            <input class="field" name="name" required />
        </p>
        <p>
            <label>Email (*)</label>
            <input class="field" name="email" type="email" required />
        </p>
        <p>
            <label><?php _e('Telephone', 'ns');?> (*)</label>
            <input class="field" name="phone" required />
        </p>
        <p>
            <label><?php _e('VIN', 'ns');?> (*)</label>
            <input class="field" name="vinno" required />
        </p>
        <p>
            <label></label>
            <input type="hidden" name="action" value="submit_form_vehicle_info_ajax" />
            <input type="hidden" name="nonce" value="<?php echo wp_create_nonce('submit_form_vehicle_info_none');?>" />
            <button type="submit" class="button"><?php _e('Send', 'ns');?></button>
        </p>
    </div>

    <div class="form-group alert hidden clearfix">
        <ul class="arlet-list"></ul>
    </div>
</form>

<script type="text/javascript">
    jQuery(document).ready(function($) {

        /*submit form info*/
        $('#vehicle_info_form').submit(function(event) {
            event.preventDefault();
            var form = $(this);
            var form_button = form.find('button');
            var form_button_text = form_button.text();

            $.ajax({
                url: wp_vars.ajaxurl,
                type: 'POST',
                dataType: 'json',
                data: $(form).serialize(),
                beforeSend: function () {
                    form_button.text("Đang gửi đi . . .");
                    form_button.prop('disabled', true);
                },
                complete: function () {
                    form_button.text(form_button_text);
                    form_button.prop('disabled', false);
                }
            })
            .done(function (response) {
                if(response.success) {
                    form.find('.form-fields').html('');
                }

                if (response.msg) {
                    form.find('.alert').show();
                    form.find('.alert > ul').text(response.msg);
                }
            })
            .fail(function () {
                console.log("Save data failed!");
            });
         });
    });

</script>