<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */


/**
 * Use this to replace base url in database:
 *
UPDATE wp_posts SET post_content = REPLACE(post_content,'http://old','http://new');
UPDATE wp_options SET option_value = REPLACE(option_value,'http://old','http://new');
 */

/** The name of the database for WordPress */
define('DB_NAME', 'vh-green-villas');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/** Disallow admin to edit plugins, theme */
define( 'DISALLOW_FILE_EDIT', true );
define( 'DISALLOW_FILE_MODS', true );
define('WP_POST_REVISIONS', false );
/** Disable auto update core */
define( 'WP_AUTO_UPDATE_CORE', false );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 *
 * Use this to generate key: https://api.wordpress.org/secret-key/1.1/salt/
 */
define('AUTH_KEY',         'i_%^^P%OBcGzCj!ewV}I`NvnF3a+7^*ZK]tNtHs3**S3l>.M2>q{0kXcNN|y/|Z]');
define('SECURE_AUTH_KEY',  'HkD{YW5j?6Kfg5M2%6REb|p.zz #d044@V]Ra@#z0}Qh3K1 h=^ej!vs v>E4au|');
define('LOGGED_IN_KEY',    'BNkt,O|tX2];tmLLF{)R1jc?J[{PR.inPh~d?(}TmAXLi5p%;1Bd8CHDzRYn}Yxk');
define('NONCE_KEY',        'c+Ek}kTT4$+Egga8HGyv2>-~d;:C;w<K}}S}+wGqrXkSq:M))1V40X(NPTOhoN  ');
define('AUTH_SALT',        '@::`KL:Xaz@QBlu-HCxR+vXa1v15k%p@W)/gdlI-M6~~L-PA#x:;:+kfuRw0k}5<');
define('SECURE_AUTH_SALT', 'brkVL29!r)k+N1gY+{Ut(@5+]sT+@+4^ln(aCuv`y8tZs&rFx]6U<c+*TK I0@I7');
define('LOGGED_IN_SALT',   ')%O,We-hWINTRn-c$EKf(d=^>)}HZu)YL^/<4ji:s:U9DLjd3|aT04,S9HvzK7=n');
define('NONCE_SALT',       'yj$);<=I},giJXHaG7FJMn-P:3^7vnHF0qA8uRm]C87,DeB24`@5H=yu;rsKAj+C');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'tu_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
    define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');