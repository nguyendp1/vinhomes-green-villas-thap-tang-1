<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */


/**
 * Use this to replace base url in database:
 *
UPDATE wp_posts SET post_content = REPLACE(post_content,'http://old','http://new');
UPDATE wp_options SET option_value = REPLACE(option_value,'http://old','http://new');
 */

/** The name of the database for WordPress */
define('DB_NAME', 'develop_gpark');

/** MySQL database username */
define('DB_USER', 'develop_gpark');

/** MySQL database password */
define('DB_PASSWORD', 'BAe4bqHUWh');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/** Disallow admin to edit plugins, theme */
define( 'DISALLOW_FILE_EDIT', true );
define( 'DISALLOW_FILE_MODS', true );
define('WP_POST_REVISIONS', false );
/** Disable auto update core */
define( 'WP_AUTO_UPDATE_CORE', false );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 *
 * Use this to generate key: https://api.wordpress.org/secret-key/1.1/salt/
 */
define('AUTH_KEY',         'y?dPskAG<2}b[^rK|YI# >EQ?V|^*#t$bgNYts,Hm@ykM5waQ[T,:(gU;n>NY|lE');
define('SECURE_AUTH_KEY',  'v`e!Y[)>h)*kvc,&*I*Kkjc~Wka!s!gArikn*Pz9wavA4br^B{Z7kt-mal5](Aa6');
define('LOGGED_IN_KEY',    '({ rm>?yib[-r4Fy?/rB=n8Q+B@4H=ZJb^|VTu&/;q-SpQ;ysM^#!6FDYS1806+_');
define('NONCE_KEY',        '^v+t+5sNO@P2`9P2]}/^_XFg8Ly|Fz<cA%}:k_p!PUfT[+I0h-f+0,Z]%A6!$JBB');
define('AUTH_SALT',        'XHor4<WE*#s$!D1|h+0K?JQ z4m1%]q&+Iz9+Y)-q^K|3ypPp-3ph./w{^V.+E@+');
define('SECURE_AUTH_SALT', '0$Da:~64=~m|Z^Bk&J&jc$/~>.pxm.mj#f@f@2cyA9Zi6/BE,=UxA!,3y!x;h|$V');
define('LOGGED_IN_SALT',   'a{TAL* Ug7%k8G<sxAbWz*9w$j{= *JT~UBK4gfv``kP-[y-^I&<&+tO(/1_os$%');
define('NONCE_SALT',       'R{};qIt~`iyqMsvdZ!I^VO_ag^.>e}|A|3P4%<Fm&9/d;n@U&X%n[+geQqiZ4(wB');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'tu_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
    define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');