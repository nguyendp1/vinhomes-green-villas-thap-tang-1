﻿	<div id="footer">
		<div class="top-footer">
			<div class="container fluid clearfix">
				<ul class="lsn clearfix">
					<li class="grid1-5"><a href="download.php" onClick="ga('send', 'event', { eventCategory: 'Link', eventAction: 'click', eventLabel: 'Click Download'});">Tài liệu bán hàng</a></li>
					<li class="grid1-5"><a href="http://vinhomes.vn/mua-ban/du-an-moi/chi-tiet/du-an-vinhomes-central-park/tien-do-thi-cong" target="blank">Tiến độ thi công</a></li>
					<li class="grid1-5"><a href="https://www.youtube.com/watch?v=ksYcqtny-jw">Video clip</a></li>
					<li class="grid1-5"><a href="http://vinhomes.vn/tin-tuc-su-kien/tin-tuc" target="blank">Tin tức</a></li>
					<li class="grid1-5"><a href="https://www.facebook.com/vh.centralpark?fref=ts" onClick="ga('send', 'event', { eventCategory: 'Link', eventAction: 'click', eventLabel: 'Click Facebook'});">Facebook</a></li>
					<li class="grid1-5"><a href="https://www.instagram.com/vinhomes.jsc" target="blank">Instagram</a></li>
				</ul>
			</div>
		</div>
		<div class="bot-footer">
			<div class="container fluid clearfix">
				<div class="grid2">
					<a class="fr" href="/"><img src="images/logo-footer.png" alt=""></a>
				</div>
				<div class="grid10 clearfix">
					<div class="grid7">
						<p>Địa chỉ dự án: Khu Tân Cảng, 720A Điện Biên Phủ, Phường 22,</p>
						<p>Quận Bình Thạnh, TP. Hồ Chí Minh</p>
						<p>Điện thoại: <a href="tel:0909366006" onClick="ga('send', 'event', { eventCategory: 'Link', eventAction: 'click', eventLabel: 'Click Hotline'});">0909 366 006</a></p>
						<!-- <p>Fax: <a href="tel:0909366006">0909 366 006</a></p> -->
					</div>
					<div class="grid5">
						<p>Địa chỉ giao dịch: 208 Nguyễn Hữu Cảnh, Phường 22,</p>
						<p>Quận Bình Thạnh, TP. Hồ Chí Minh</p>
						<p>Điện thoại: <a href="tel:0909366006" onClick="ga('send', 'event', { eventCategory: 'Link', eventAction: 'click', eventLabel: 'Click Hotline'});">0909 366 006</a></p>
						<!-- <p>Fax: <a href="tel:0909366006">0909 366 006</a></p> -->
					</div>

					<p>&copy; Vinhomes Central Park 2015, All Rights Reserved. <!-- Site by <a href="http://timeuniversal.vn/">Time Universal</a> --></p>
				</div>
			</div>
		</div>
	</div>

	<?php /* ?>
	<!-- Google Tag Manager -->
	<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-N2XB2K"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-N2XB2K');</script>
	<!-- End Google Tag Manager -->
	<?php */ ?>
</body>
</html>