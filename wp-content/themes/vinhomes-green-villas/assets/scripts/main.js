"use strict";

/**
 * Plugin Tabs 
 * Require jQuery
 */
(function ($) {
    $.fn.tabs = function () {
        var wrap = $(this);
        var head = wrap.find('[data-head]');
        var content = wrap.find('[data-content]');
        this.reset = (function () {
            head.not(head.first()).removeClass('is-active');
            content.not(content.first()).hide();
        }).call(this);

        this.headClick = head.click(function (event) {
            event.preventDefault();

            if ($(this).hasClass('is-active')) {
                return false;
            }

            var content_target = $(this).attr('href');

            head.removeClass('is-active');
            content.hide();

            $(this).addClass('is-active');
            $(content_target).fadeIn();
        });

        return this;
    };
})(jQuery);

jQuery(document).ready(function ($) {
    /**
     * Format phone number || All tags has attribute tel
     */
    $('a[href^="tel:"]').attr('href', function(_,v){
        return v.replace(/\(0\)|\s+/g,'')
    });
});

  /*Check validate form */
  function hasNumber(myString) {
    return /\d/.test(myString);
}

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

function validatePhonenumber(number) {
    var re = /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/ ;
    return re.test(String(number));
}
