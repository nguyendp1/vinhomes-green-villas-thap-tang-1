<?php wp_footer(); ?>
<?php
$option_link_facebook = get_option('option_link_facebook') ?: 'javascript:void(0)';
$option_link_youtube = get_option('option_link_youtube') ?: 'javascript:void(0)';
$option_link_instagram = get_option('option_link_instagram') ?: 'javascript:void(0)';
$option_link_zalo = get_option('option_link_zalo') ?: 'javascript:void(0)';
$hotline = get_option('option_phone') ?: 'javascript:void(0)';
$option_email = get_option('option_email') ?: 'javascript:void(0)';
$option_address = get_option('option_address') ?: 'javascript:void(0)';
$option_address_project = get_option('option_address_project') ?: 'javascript:void(0)';
$option_link_adayroi = get_option('option_link_adayroi') ?: 'javascript:void(0)';
?>
<footer>
	<div class="up">
		<p class="note">* Thông tin, hình ảnh, các tiện ích trên website này chỉ mang tính chất minh hoạ tương đối và có thể được điều chỉnh theo quyết định của Chủ đầu tư tại từng thời điểm đảm bảo phù hợp quy hoạch và thực tế thi công dự án. Các thông tin, cam kết chính thức sẽ được quy định cụ thể tại Hợp đồng mua bán. Việc quản lý, vận hành và kinh doanh của khu đô thị sẽ theo quy định của Ban quản lý.</p>
		<div class="page">
			<a href="#" class="item-page" data-aos="zoom-in" data-aos-duration="1000" data-aos-anchor="footer" >
				<img src="<?php echo IMAGE_URL.'/home/s123/footer-1.png'; ?>" alt="">
				<p>Tài liệu bán hàng</p>
			</a>
			<a href="#" class="item-page" data-aos="zoom-in" data-aos-duration="1500" data-aos-anchor="footer" >
				<img src="<?php echo IMAGE_URL.'/home/s123/footer-2.png'; ?>" alt="">
				<p>Tiến độ thi công</p>
			</a>
			<a href="#" class="item-page" data-aos="zoom-in" data-aos-duration="2000" data-aos-anchor="footer" >
				<img src="<?php echo IMAGE_URL.'/home/s123/footer-3.png'; ?>" alt="">
				<p>Chính sách bán hàng</p>
			</a>
		</div>
	</div>
	<div class="down">
		<a href="<?php echo HOME_URL; ?>" class="logo-footer" data-aos="zoom-in" data-aos-duration="1000" data-aos-anchor="footer">
			<?php include_once (TEMPLATE_PATH.'/assets/images/home/s123/logo-footer.svg'); ?> 
		</a>
		<div class="contact" data-aos="zoom-in" data-aos-duration="1500" data-aos-anchor="footer">
			<div class="link">
				<a href="<?php echo $option_link_facebook;?>" target= "_blank" data-event="auto" data-event-category="Social" data-event-action="click" data-event-label="Facebook"><i class="fa fa-facebook-square"></i></a> 
				<a href="<?php echo $option_link_youtube;?>" target= "_blank" data-event="auto" data-event-category="Social" data-event-action="click" data-event-label="Youtube"><i class="fa fa-youtube-play"></i></a>
				<a href="<?php echo $option_link_instagram;?>" target= "_blank" data-event="auto" data-event-category="Social" data-event-action="click" data-event-label="Instagram"><i class="fa fa-instagram"></i></a>
				<a href="<?php echo $option_link_zalo;?>" target= "_blank" data-event="auto" data-event-category="Social" data-event-action="click" data-event-label="Zalo"><img src="<?php echo IMAGE_URL.'/home/s123/zalo.svg'; ?>" alt=""></a>
				<a href="<?php echo $option_link_adayroi;?>" target= "_blank" data-event="auto" data-event-category="Social" data-event-action="click" data-event-label="Adayroi">Adayroi</a>
			</div>
			<div class="item-footer hotline"> 
				<p>Hotline:</p>
				<p><a href="tel:<?php echo $hotline;?>" class="_phone" data-event="auto" data-event-category="Hotline" data-event-action="click" data-event-label="18001036"><?php echo $hotline;?></a></p>
			</div>
			<div class="item-footer">
				<p>Email:</p>
				<p><a href="mailto:<?php echo $option_email;?>" class="_email" data-event="auto" data-event-category="Email" data-event-action="click"><?php echo $option_email;?></a></p>  
			</div>
		</div>
		<div class="adress" data-aos="zoom-in" data-aos-duration="2000" data-aos-anchor="footer"> 
			<div class="item-footer">
				<p>Địa chỉ giao dịch:</p>
				<p><?php echo $option_address;?></p>
			</div>
			<div class="item-footer">
				<p>Địa chỉ dự án:</p>
				<p><?php echo $option_address_project;?></p>
			</div> 
		</div>
	</div>
</footer>
<script> 
	jQuery(document).ready(function($) {
		AOS.init({
			offset: 200,
			duration: 950,
			easing: 'ease-in-out',
			disable: 'mobile'
		});

		//js xu ly fancybox s7
		$('.fancybox_library').fancybox();
	});  
</script>
</body>
</html>