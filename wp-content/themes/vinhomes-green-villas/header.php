<?php $_siteInfo = array(
'app_id' => FACEBOOK_APP_ID,// App Name: TU
'type' => 'website',// Need to be changed with each type of pages
'title' => get_bloginfo('name'),// Site Title
'url' => home_url(),// Permalink
'image' => get_template_directory_uri() . '/screenshot.jpg?v='.date('dmY'),// Screenshot, Thumbnail
'description' => get_bloginfo('description'),// Tagline, Excerpt
'author' => 'Time Universal',// Change manually
);

if( is_tax() || is_category() || is_tag() ) {
    if( !is_category() )
        $_siteInfo['url'] = get_term_link( get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
    else
        $_siteInfo['url'] = get_category_link( get_query_var( 'cat' ) );

    $_siteInfo['title'] = single_term_title( '', false ) .' | '. get_bloginfo('name');
    $_siteInfo['description'] = term_description() ? term_description() : $_siteInfo['description'];
    $_siteInfo['type'] = 'article';
}

if( is_search() ) {
    $_siteInfo['title'] = 'Search: '. esc_html( get_query_var('s') ) .' | '. get_bloginfo('name');
    $_siteInfo['description'] = 'Search result for "'. esc_html( get_query_var('s') ) .'" from '. get_bloginfo('name');
}

if( is_author() ) {
    $authorID = get_query_var('author');
    $authorData = get_userdata( $authorID );
    $_siteInfo['title'] = $authorData->display_name .' @ '. get_bloginfo('name');
    $_siteInfo['description'] = $authorData->description ? $authorData->description : $_siteInfo['description'];
}

if( is_single() || is_page() ) {
    $imageSource = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
    $postDescription = $post->post_excerpt ? $post->post_excerpt : wp_trim_words( $post->post_content );
    $_siteInfo['title'] = $post->post_title;
    $_siteInfo['description'] = $postDescription ? strip_tags( $postDescription ) : $_siteInfo['description'];
    $_siteInfo['image'] = $imageSource ? $imageSource[0] : $_siteInfo['image'];
    $_siteInfo['url'] = get_permalink( $post->ID );
    $_siteInfo['type'] = 'article';
// $_siteInfo['author'] = get_the_author_meta( 'display_name', $post->post_author );
}

if( is_paged() ) {
    $_siteInfo['title'] .= ' | '.__('Trang', TEXT_DOMAIN).' '. get_query_var('paged');
    $_siteInfo['description'] .= ' | '.__('Trang', TEXT_DOMAIN).' '. get_query_var('paged');
}
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

    <meta name="author" content="<?php echo str_replace('"', '', $_siteInfo['author']);?>" />
    <meta name="description" content="<?php echo str_replace('"', '', $_siteInfo['description']);?>" />
    <meta property="fb:app_id" content="<?php echo $_siteInfo['app_id'];?>" />
    <meta property="og:type" content='<?php echo $_siteInfo['type'];?>' />
    <meta property="og:title" content="<?php echo str_replace('"', '', $_siteInfo['title']);?>" />
    <meta property="og:url" content="<?php echo $_siteInfo['url'];?>" />
    <meta property="og:image" content="<?php echo $_siteInfo['image'];?>" />
    <meta property="og:description" content="<?php echo str_replace('"', '', $_siteInfo['description']);?>" />
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700" rel="stylesheet">
    <link type="image/x-icon" rel="shortcut icon" href="<?php echo IMAGE_URL; ?>/favicon.png">

    <title><?php echo esc_html( $_siteInfo['title'] ); ?></title>

    <?php wp_head();?>

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PDQ4KNN');</script>
<!-- End Google Tag Manager -->

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-7565194-43"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-7565194-43');
</script>
</head>
<body <?php body_class(); ?>>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PDQ4KNN"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->
        <?php if( is_home() ): ?>
        <?php endif; ?>
        <header class="">
            <div class="logo">
                <a href="<?php echo HOME_URL; ?>"><img src="<?php echo IMAGE_URL.'/home/s123/logo-header.svg'; ?>" alt=""></a>
            </div>
            <ul class="menu">
                <li><a href="">giới thiệu</a></li>
                <li><a href="">thư viện</a></li>
                <li><a href="#">chính sách</a></li>
                <li><a href="#">tiện ích</a></li>
                <li><a href="#">tiến độ</a></li>
                <li><a href="#">tin tức</a></li>
                <li><a href="#">liên hệ</a></li> 
<!-- <li class="language"> 
<a href="#" class="active">vn</a>
<a>en</a>
</li> -->
</ul> 
</header>

<div class="social"> 
    <a href="#" data-event="auto" data-event-category="Social" data-event-action="click" data-event-label="Facebook"><i class="fa fa-facebook"></i></a>
    <a href="tel:0888049669" class="_
    phone" data-event="auto" data-event-category="Hotline" data-event-action="click" data-event-label="0888049669"><i class="fa fa-phone"></i></a> 
    <a href="mailto:info@grand-worldphuquoc.vn" class="_email" data-event="auto" data-event-category="Email" data-event-action="click"><i class="fa fa-envelope"></i></a>
</div>

<div class="menu-mobile" style="display: none;">
    <img src="<?php echo IMAGE_URL.'/home/s123/menu-bg-mobile.png'; ?>" alt="" class="background">

    <ul class="menu">
        <li class="menu_item"><a href="£">giới thiệu</a></li>
        <li class="menu_item"><a href="£">thư viện</a></li>
        <li class="menu_item"><a href="#">chính sách</a></li>
        <li class="menu_item"><a href="#">tiện ích</a></li>
        <li class="menu_item"><a href="#">tiến độ</a></li>
        <li class="menu_item"><a href="#">tin tức</a></li>
        <li class="menu_item"><a href="#">liên hệ</a></li> 
<!-- <li class="language"> 
<a href="#" class="active">vn</a>
<a>en</a>
</li> -->
</ul> 
</div>
<div class="logo mobile">
    <a href="#"><img src="<?php echo IMAGE_URL.'/home/s123/logo-header.svg'; ?>" alt=""></a>
</div>
<div class="btn">
    <span>menu</span>
    <span style="display: none">exit</span>
    <span></span>
</div>
<div class="_bg-menu-mobile"></div>
<script>
    jQuery(document).ready(function($) {

        $('.btn').click(function(event) {
            $('.menu-mobile').toggleClass('click');
            $('.btn').toggleClass('click');
            $('.logo').toggleClass('click');
        }); 
        if($( window ).width() > 769){
            $(window).scroll(function () {
                var y = $(window).scrollTop(); 
                if (y > 20) { 
                    $('header').addClass('scroll-menu');  
                }  
                else{
                    $('header').removeClass('scroll-menu'); 
                }  
            }); 
        };   
        if($( window ).width() < 769){
            $('header').addClass('scroll-menu'); 
        };         
        $(window).scroll(function () {
            var y = $(window).scrollTop(); 
            if(y >  $('._1').offset().top){
                $('.social').addClass('active'); 
            } 
            if(y < $('._2').offset().top){
                $('.social').removeClass('active'); 
            } 
        });
    });
</script>