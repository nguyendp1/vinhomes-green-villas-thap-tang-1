<?php

/**
 * Adding scripts and styles
 * All your scripts and styles will be included in wp_head()
 */
add_action('wp_enqueue_scripts', 'tu_enqueue_scripts_styles');

function tu_enqueue_scripts_styles() {

    if (wp_script_is('media')) {
        wp_enqueue_media();
    }

    wp_enqueue_style('style', get_stylesheet_uri());
    wp_enqueue_style('wow', TEMPLATE_URL.'/assets/bower_components/wow/css/libs/animate.css');
    wp_enqueue_style('aos', TEMPLATE_URL.'/assets/bower_components/aos/dist/aos.css');
    wp_enqueue_style('font-awesome', TEMPLATE_URL.'/assets/bower_components/font-awesome/css/font-awesome.min.css');
    wp_enqueue_style('swiper', TEMPLATE_URL . '/assets/bower_components/Swiper/dist/css/swiper.min.css');
    wp_enqueue_style('fancybox', TEMPLATE_URL.'/assets/bower_components/fancybox/dist/jquery.fancybox.min.css');
    wp_enqueue_style('slick', TEMPLATE_URL.'/assets/bower_components/slick-carousel/slick/slick.css');
    wp_enqueue_style('slick', TEMPLATE_URL.'/assets/bower_components/slick-carousel/slick/slick-theme.css');
    wp_enqueue_style('partials', TEMPLATE_URL.'/assets/stylesheets/sass/dist/partials.css');
    wp_enqueue_style('main', TEMPLATE_URL.'/assets/stylesheets/sass/dist/main.css');

    wp_enqueue_script('jquery');
    wp_enqueue_script('wow', TEMPLATE_URL . '/assets/bower_components/wow/dist/wow.min.js', array(), '0.1', false);
    wp_enqueue_script('aos', TEMPLATE_URL . '/assets/bower_components/aos/dist/aos.js', array(), '0.1', false);
    wp_enqueue_script('swiper', TEMPLATE_URL . '/assets/bower_components/Swiper/dist/js/swiper.min.js', array(), '0.1', false);
    wp_enqueue_script('fancybox', TEMPLATE_URL . '/assets/bower_components/fancybox/dist/jquery.fancybox.min.js', array(), '0.1', false);
    wp_enqueue_script('slick', TEMPLATE_URL . '/assets/bower_components/slick-carousel/slick/slick.js', array(), '0.1', false);
    wp_enqueue_script('panzoom', TEMPLATE_URL.'/assets/bower_components/jquery.panzoom/dist/jquery.panzoom.min.js');
    wp_enqueue_script('scripts', TEMPLATE_URL . '/assets/scripts/main.js', array(), '0.1', false);

    $wp_script_data = array(
        'ADMIN_AJAX_URL' => ADMIN_AJAX_URL,
        'HOME_URL' => HOME_URL
    );

    wp_localize_script('scripts', 'wp_vars', $wp_script_data);
}
