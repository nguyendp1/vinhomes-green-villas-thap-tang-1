<?php
/**
 * Duplicate multiple post with parameter is post already exists.
 * 
 * @see   0.1.0
 * 
 * @param   object      $post   The a post already exists
 * @param   integer     $lengt  Number post
 * @return  false|array         Array containing ids of new post. False on failure
 */

if ( !function_exists('wp_duplicate_post') ) :

    function wp_duplicate_post($post, $length) {
        if ( !is_object($post) ) {
            return false;
        }

        $posts_id  = [];
        $thumbnail = get_post_meta($post->ID, '_thumbnail_id', true);

        $agrs = array (
            'post_title'   => $post->post_title,
            'post_content' => $post->post_content,
            'post_excerpt' => $post->post_excerpt,
            'post_type'    => $post->post_type,
            'post_status'  => $post->post_status,
        );
        
        for ( $i = 0; $i < $length; $i++ ) {
            $new_post = wp_insert_post($agrs);

            if ( !is_wp_error($new_post) ) {
                update_post_meta($new_post, '_thumbnail_id', $thumbnail);
                array_push($posts_id, $new_post);

                continue;
            }
        }

        return $posts_id;
    }

else :
    echo "Function wp_duplicate_post are exists.";
endif;