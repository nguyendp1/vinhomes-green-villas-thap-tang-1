<?php 
get_header(); 
$home = tu_get_home_with_pagination(1, -1);
?> 
<main>
	<?php if ( $home->have_posts() ) : ?>
		<?php while ( $home->have_posts() ) : $home->the_post(); ?>
			<?php
			$post_id = get_the_ID();
			$screen_2_label = get_post_meta($post_id, 'screen_2_label', true);
			$screen_2_desc = get_post_meta($post_id, 'screen_2_desc', true);
			$screen_2_image = get_post_meta($post_id, 'screen_2_image', true);
			$screen_3_label = get_post_meta($post_id, 'screen_3_label', true);
			$screen_3_desc = get_post_meta($post_id, 'screen_3_desc', true);
			$screen_4_label = get_post_meta($post_id, 'screen_4_label', true);
			$screen_4_desc = get_post_meta($post_id, 'screen_4_desc', true);
			$screen_5_label = get_post_meta($post_id, 'screen_5_label', true);
			$screen_5_desc = get_post_meta($post_id, 'screen_5_desc', true);
			$screen_5_link = get_post_meta($post_id, 'screen_5_link', true);
			$screen_5_image = get_post_meta($post_id, 'screen_5_image', true);
			$screen_6_label = get_post_meta($post_id, 'screen_6_label', true);
			$screen_6_desc = get_post_meta($post_id, 'screen_6_desc', true);
			$screen_7_image = get_post_meta($post_id, 'screen_7_image', true);
			$screen_7_link = get_post_meta($post_id, 'screen_7_link', true);
			$screen_8_label = get_post_meta($post_id, 'screen_8_label', true);
			$screen_8_desc = get_post_meta($post_id, 'screen_8_desc', true);
			$screen_8_hotline = get_post_meta($post_id, 'screen_8_hotline', true);
			$screen_8_note = get_post_meta($post_id, 'screen_8_note', true);
			?>
			<?php include_once (TEMPLATE_PATH. '/partials/home/section_1.php');?>
			<?php include_once (TEMPLATE_PATH. '/partials/home/section_2.php');?>
			<?php include_once (TEMPLATE_PATH. '/partials/home/section_3.php');?>
			<?php include_once (TEMPLATE_PATH. '/partials/home/section_4.php');?>
			<?php include_once (TEMPLATE_PATH. '/partials/home/section_5.php');?>
			<?php include_once (TEMPLATE_PATH. '/partials/home/section_6.php');?>
			<?php include_once (TEMPLATE_PATH. '/partials/home/section_7.php');?>
			<?php include_once (TEMPLATE_PATH. '/partials/home/section_8_1.php');?>
			<?php include_once (TEMPLATE_PATH. '/partials/home/section_8.php');?>
			
		<?php endwhile; ?>
	<?php endif; ?>	
</main> 
<script> 
	jQuery(document).ready(function($) {
		//js xu ly zoom img s6
		var section = $('#img_panzoom');
		var panzoom = section.find('.panzoom').panzoom({
			$zoomIn: section.find("._zoom_in"),
			$zoomOut: section.find("._zoom_out"),
			startTransform: "scale(1)",
			increment: 0.1,
			minScale: 1.15,
			maxScale: 4,
			contain: 'invert',
		}).panzoom('zoom');
		panzoom.parent().on('mousewheel.focal', function(e){
			e.preventDefault();
			var delta = e.delta || e.originalEvent.wheelDelta;
			var zoomOut = delta ? delta < 0 : e.originalEvent.deltaY > 0;
			panzoom.panzoom('zoom', zoomOut, {
				increment: 0.1,
				animate: false,
				focal: e
			});
		});

		//js xu ly silde s7 pc
		var swiper_s7 = new Swiper('#swc_s7', {
			spaceBetween: 0,
			speed: 1200,
			loop:true,
			pagination: {
				el: '#swp_s7',
				dynamicBullets: true,
			},
		});

		//js xu ly silde s7 mobile
		var swiper_s7 = new Swiper('#swc_s7_m', {
			spaceBetween: 4.2,
			speed: 1200,
			loop:true,
			pagination: {
				el: '#swp_s7_m',
				dynamicBullets: true,
			},
		});

		//js xu ly s8.1
		if($('._text_list ._text_item').hasClass('selected')){
			$('._input_text span').text($('._text_list ._text_item.selected').text());
			var data_link = $('._text_list ._text_item.selected').attr('data-link');
			// console.log(data_link);
			$('._button_download').attr('href',data_link);
			// link_download = data_link;
		};
		$('._text_item').click(function(){
			var data_link = $(this).attr('data-link');
			$('._button_download').attr('href',data_link);
			$('._text_item').removeClass('selected');
			$(this).addClass('selected');
			$('._input_text span').text($('._text_list ._text_item.selected').text());
			$('._text_list').slideUp(100);
		});
		$('._input_text').click(function(){
			$('._text_list').slideToggle(100);
		});

		$(document).click(function(e){
			var data_down = $('._text_list').attr('style');
			if(data_down == 'display: block;'){
				$('._text_list').hide();
			}
		});

	});  
</script> 
<?php get_footer(); ?>

