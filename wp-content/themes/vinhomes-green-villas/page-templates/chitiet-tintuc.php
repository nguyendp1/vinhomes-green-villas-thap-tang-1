<?php //  Template Name: Tin tức chi tiết ?>
<?php get_header(); ?>
<div class="chitiettintuc">
	<div class="baner" style="background-image:url('<?php echo IMAGE_URL.'/chitiettintuc/Mask Group.png'; ?>')">
	</div>
	<div class="colum">
		<div class="left">
			<a href="#"> <i class="fa fa-angle-left" aria-hidden="true"></i> Trở lại danh sách</a>
			<h1>
				Ra mắt top 98 biệt thự 5 sao Vinhomes Green Villas
			</h1>
			<div class="text">
				<p>
					14 tháng 4, 2019 -
				</p>
				<a href="#">Tin Dự Án</a>
			</div>
			<div class="content">
				<div class="title">
					Nullam vestibulum semper risus, at pulvinar massa eleifend eget. Duis aliquam vulputate tellus, ac tristique metus semper vel.
				</div>
				<p>
					<img src="<?php echo IMAGE_URL . '/chitiettintuc/Mask Group (1).png'; ?>')">
				</p>
				<p>
					In nunc justo, accumsan at velit molestie, blandit convallis dui. Curabitur ac nisl commodo, consequat ex at, luctus arcu. Curabitur vel condimentum sapien, vel fermentum nunc. Nullam eget volutpat lacus. Nullam sit amet ex commodo, lacinia lectus at
				</p>
				<div class="quote">
					<i class="fa fa-quote-left" aria-hidden="true"></i>
					<p>
						Duis aliquam vulputate tellus, ac tristique metus semper velliquam accumsan, lectus non aliquam semper.
					</p>
				</div>
				<p>
					Nulla in nulla tincidunt, condimentum turpis in, tempus dui. Donec lacus sapien, luctus a arcu sit amet, tincidunt aliquam sem. Nam porttitor metus non dui rhoncus, nec placerat arcu vestibulum. Cras scelerisque augue enim, placerat egestas mi elementum sit amet. Fusce luctus ornare elit, at laoreet erat varius ac.
				</p>
				<p>
					Sed posuere maximus ex, eu convallis nunc fringilla in. Praesent ut eros bibendum, tincidunt neque ut, gravida erat. Fusce dapibus ullamcorper convallis.
				</p>
				<p>
					Cras fringilla blandit eleifend. Etiam vitae lacus fermentum metus finibus venenatis porta at mauris. Vivamus bibendum augue accumsan tellus fermentum, vitae aliquam mi laoreet. Proin gravida egestas metus a consectetur.
				</p>
				<div class="evaluate">
					<div class="stars">
						<i class="fa fa-star-o active" aria-hidden="true"></i>
						<i class="fa fa-star-o" aria-hidden="true"></i>
						<i class="fa fa-star-o" aria-hidden="true"></i>
						<i class="fa fa-star-o" aria-hidden="true"></i>
						<i class="fa fa-star-o" aria-hidden="true"></i>
					</div>
					<div class="text">
						<p>5.0</p>
						<a href="#">3 đánh giá</a>
					</div>
				</div>
			</div>
			<div class="bottom">
				<div class="hastag">
					<a  href="#">#vinhomes</a>
					<a  href="#">#duan</a>
					<a  href="#">#canho</a>
				</div>
				<div class="social-chitiettintuc">
					<p> Chia sẻ :</p>
					<a href="#">
						<div class="img" style="background-image:url('<?php echo IMAGE_URL.'/chitiettintuc/facebook.png'; ?>')"></div>
					</a>
					<a href="#">
						<div class="img" style="background-image:url('<?php echo IMAGE_URL.'/chitiettintuc/twitter.png'; ?>')"></div>
					</a>
					<a href="#">
						<div class="img" style="background-image:url('<?php echo IMAGE_URL.'/chitiettintuc/linkedin.png'; ?>')"></div>
					</a>
					<a href="#">
						<div class="img" style="background-image:url('<?php echo IMAGE_URL.'/chitiettintuc/youtube.png'; ?>')"></div>
					</a>
				</div>
			</div>
		</div>
		<div class="right">
			<div class="news">
				<h3>Tin liên quan</h3>
				<a href="#" class="item">
					<div class="img"style="background-image:url('<?php echo IMAGE_URL.'/chitiettintuc/Mask Group (2).png'; ?>')">
					</div>
					<div class="text">
						<div class="title"> Sed commodo dolor in massa vehicula 
						</div>	
						<p>14 tháng 4, 2019</p>
					</div>
				</a>	
				<a href="#" class="item">
					<div class="img"style="background-image:url('<?php echo IMAGE_URL.'/chitiettintuc/Mask Group (2).png'; ?>')">
					</div>
					<div class="text">
						<div class="title"> Sed commodo dolor in massa vehicula 
						</div>	
						<p>14 tháng 4, 2019</p>
					</div>
				</a>
				<a href="#" class="item">
					<div class="img"style="background-image:url('<?php echo IMAGE_URL.'/chitiettintuc/Mask Group (2).png'; ?>')">
					</div>
					<div class="text">
						<div class="title"> Sed commodo dolor in massa vehicula 
						</div>	
						<p>14 tháng 4, 2019</p>
					</div>
				</a>
				<a href="#" class="item">
					<div class="img"style="background-image:url('<?php echo IMAGE_URL.'/chitiettintuc/Mask Group (2).png'; ?>')">
					</div>
					<div class="text">
						<div class="title"> Sed commodo dolor in massa vehicula 
						</div>	
						<p>14 tháng 4, 2019</p>
					</div>
				</a>
				<a  class='button' href="#">
					Xem thêm
				</a>	
			</div>
		</div>
	</div>
</div>
<?php include_once (TEMPLATE_PATH. '/partials/home/section_8.php');?>
<?php get_footer(); ?>