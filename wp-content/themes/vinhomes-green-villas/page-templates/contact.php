<?php //  Template Name: Liên hệ ?>

<?php get_header(); ?>
<div class="_page_contact">
	<div class="page-title">
		<p>Liên hệ</p>
		<div class="decor">
		</div>
		<div class="breadcrumb">
			<div class="icon">
				<i class="fa fa-home" aria-hidden="true"></i>
			</div>
			<a href="#">Trang chủ /</a>
			<a href="#"class="active">Liên hệ</a>
		</div>
	</div>

	<div class="_contact_s1">
		<div class="_left">
			<?php include_once (TEMPLATE_PATH.'/assets/images/contact/title.svg'); ?> 
			<div class="_title">Thông tin liên hệ</div>
		</div>
		<div class="_right">
			<p>Địa chỉ giao dịch:</p>
			<div class="_text">TT giao dịch BĐS Royal City, tầng hầm B2 Royal City, 72a Nguyễn Trãi, Thanh Xuân, Hà Nội.</div>
			<p>Địa chỉ dự án:</p>
			<div class="_text">Trung tâm Giao Dịch Bất Động Sản TCO – Số 2 – Công ty TNHH MTV Tư vấn và Kinh doanh Bất Động Sản TCO, Lô B2-R3-03, tầng B2, Tòa nhà R3, Khu Trung tâm Thương mại, giáo dục và căn hộ Royal City, số 72A đường Nguyễn Trãi, phường Thượng Đình, quận Thanh Xuân, thành phố Hà Nội.</div>
			<span><i class="fa fa-phone" aria-hidden="true"></i><a href="tel:180001036" data-event="auto" data-event-category="Hotline" data-event-action="click" data-event-label="180001036">18000 1036</a></span>
			<span><i class="fa fa-envelope" aria-hidden="true"></i><a href="mailto:info@vinhomes.com" data-event="auto" data-event-category="Email" data-event-action="click">info@vinhomes.com</a></span>
		</div>
	</div>

	<div class="_contact_2">
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.7109538192535!2d105.81321891493225!3d21.00422058601171!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ac9b6fe870ab%3A0xbdba6fd914881f7f!2zUjIsIDcyQSBOZ3V54buFbiBUcsOjaSwgVGjGsOG7o25nIMSQw6xuaCwgVGhhbmggWHXDom4sIEjDoCBO4buZaQ!5e0!3m2!1svi!2s!4v1558326405346!5m2!1svi!2s" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
	</div>

	<div class="_contact_s3">
		<div class="_left">
			<div class="_title">Thông tin liên hệ</div>
			<div class="_text">công ty tnhh phát triển thương mại bđs newstarland</div>
		</div>
		<div class="_between">
			<div class="_address">Địa chỉ:</div>
			<div class="_text">
				<p>Tầng 2, tòa CT1 - Eco Green, 286 Nguyễn Xiển, Thanh Trì, HN | L2-R4 Royal City </p>
				<p>Tầng 2, TTTM Vincom Long Biên – Vinhomes Riverside, HN | 14A16 khu Fideco, đường Thảo Điền, phường Thảo Điền, Q2, TP. HCM</p>
			</div>
		</div>
		<div class="_right">
			<div class="_hotline">
				<p>Hotline</p>
				<p><a href="tel:0917023888" data-event="auto" data-event-category="Hotline" data-event-action="click" data-event-label="0917023888">0917 023 888</a></p>
			</div>
			<div class="_mail">
				<i class="fa fa-envelope" aria-hidden="true"></i><a href="mailto:huyenntt.ceo@newstarland.com" data-event="auto" data-event-category="Email" data-event-action="click">huyenntt.ceo@newstarland.com</a>
			</div>
		</div>
		</div>
	</div>




	<?php include_once (TEMPLATE_PATH. '/partials/home/section_8.php');?>
</div>
<?php get_footer(); ?>
<script>
	jQuery(document).ready(function ($) {

	});
</script>
