<?php //  Template Name: Giới thiệu ?>
<?php 
get_header(); 
$page_current_id = $post->ID;
$page_thumbnail_url = has_post_thumbnail($page_current_id) ? tu_get_post_thumbnail_src_by_post_id($page_current_id,'banner') : IMAGE_URL . "/intro/s1_bg.png";
$introduce = tu_get_introduce_with_pagination(1, -1);
?>
<?php if ( $introduce->have_posts() ) : ?>
    <?php while ( $introduce->have_posts() ) : $introduce->the_post(); ?>
        <?php
        $post_id = get_the_ID(); 
        $banner_title_1 = get_post_meta($post_id, 'banner_title_1', true);
        $banner_title_2 = get_post_meta($post_id, 'banner_title_2', true);
        $screen_2_title = get_post_meta($post_id, 'screen_2_title', true);
        $screen_2_desc = get_post_meta($post_id, 'screen_2_desc', true);
        $screen_2_image = get_post_meta($post_id, 'screen_2_image', true);
        $screen_3_title = get_post_meta($post_id, 'screen_3_title', true);
        $screen_3_desc = get_post_meta($post_id, 'screen_3_desc', true);
        $screen_3_image = get_post_meta($post_id, 'screen_3_image', true);
        $screen_4_title = get_post_meta($post_id, 'screen_4_title', true);
        $screen_4_desc = get_post_meta($post_id, 'screen_4_desc', true);
        $screen_4_image = get_post_meta($post_id, 'screen_4_image', true);
        $screen_5_title = get_post_meta($post_id, 'screen_5_title', true);
        $screen_5_desc = get_post_meta($post_id, 'screen_5_desc', true);
        $screen_5_image = get_post_meta($post_id, 'screen_5_image', true);
        ?>
<div class="intro_s1" style="background-image:url('<?php echo $page_thumbnail_url; ?>')">
    <div class="tab_pages" data-aos="fade-right">
        <ul>
            <li><a href="<?php echo HOME_URL; ?>"><i class="fa fa-home" aria-hidden="true"></i>Trang chủ</a></li>
            <li><a href="javascript:void(0)">Giới thiệu</a></li>
        </ul>
    </div>
    <div class="title" data-aos="zoom-in">
        <div class="_title_1"><?php echo $banner_title_1; ?></div>
        <div class="_title_2"><?php echo $banner_title_2; ?></div>
    </div>
</div>

<div class="intro_s2">
    <div class="_left_content">
        <div class="_title_svg" data-aos="fade-right">
            <?php include TEMPLATE_PATH . '/assets/images/intro/s2_Vinhomes _Green _Villas.svg' ;?>
        </div>
        <div class="_title_text" data-aos="fade-right"><?php echo $screen_2_title; ?></div>
        <div class="_text" data-aos="fade-right"><?php echo apply_filters('the_content', $screen_2_desc); ?></div>
    </div>
    <div class="_right_img">
        <?php if (isset($screen_2_image) && $screen_2_image): $i = 1;?> 
            <?php foreach ($screen_2_image as $image ) : ?>
                <?php 
                $image_id = $image['id'];
                $image_src = tu_get_image_src_by_attachment_id( $image_id, 'full' );
                if ( $i > 2 ) break;
                ?>
                <div class="_img_left" data-aos="<?php echo $active = ($i == 1) ? 'fade-up' : 'fade-down' ;?>" style="background-image:url('<?php echo $image_src; ?>')"></div>
                <?php $i++; ?>
            <?php endforeach; ?>
        <?php endif ?>
        <!-- <div class="_img_left" data-aos="fade-up" style="background-image:url('<?php //echo IMAGE_URL.'/intro/s2_img_left.png'; ?>')"></div> -->
        <!-- <div class="_img_right" data-aos="fade-down" style="background-image:url('<?php //echo IMAGE_URL.'/intro/s2_img_right.png'; ?>')"></div> -->
        <div class="_group"data-aos="zoom-in"></div>
    </div>
</div>

<div class="intro_s3">
    <div class="_title_svg_m" data-aos="fade-down">
        <?php include TEMPLATE_PATH . '/assets/images/intro/s2_Vinhomes _Green _Villas.svg' ;?>
    </div>
    <div class="_title_svg" data-aos="fade-down">
        <?php include TEMPLATE_PATH . '/assets/images/intro/s3_Vinhomes _Green _Villas.svg' ;?>
    </div>
    <div class="_title_text" data-aos="fade-down"><?php echo $screen_3_title; ?></div>
    <div class="_text" data-aos="fade-down"><?php echo apply_filters('the_content', $screen_3_desc); ?></div>
    <div class="_slide_img" data-aos="fade-up">
        <div class="swiper-container" id="swc_intro">
            <div class="swiper-wrapper">
                <?php if (isset($screen_3_image) && $screen_3_image): ?> 
                    <?php foreach ($screen_3_image as $image ) : ?>
                        <?php 
                        $image_id = $image['id'];
                        $image_src = tu_get_image_src_by_attachment_id( $image_id, 'full' );
                        ?>
                        <div class="swiper-slide" style="background-image:url('<?php echo $image_src; ?>')"></div>
                    <?php endforeach; ?>
                <?php endif ?>
            </div>
            <div class="swiper-button-next" id="swc_intro_next"></div>
            <div class="swiper-button-prev" id="swc_intro_prev"></div>
        </div>
        <div class="swiper-pagination pagi-default" id="swp_intro"></div>
    </div>
</div>

<div class="intro_s4 intro_s2">
    <div class="_left_content">
        <div class="_title_svg" data-aos="fade-left">
            <?php include TEMPLATE_PATH . '/assets/images/intro/s2_Vinhomes _Green _Villas.svg' ;?>
        </div>
        <div class="_title_text" data-aos="fade-left"><?php echo $screen_4_title; ?></div>
        <div class="_text" data-aos="fade-left">
            <?php echo apply_filters('the_content', $screen_4_desc); ?>
        </div>
    </div>
    <div class="_right_img">
        <?php if (isset($screen_4_image) && $screen_4_image): $i = 1;?> 
            <?php foreach ($screen_4_image as $image ) : ?>
                <?php 
                $image_id = $image['id'];
                $image_src = tu_get_image_src_by_attachment_id( $image_id, 'full' );
                if ( $i > 2 ) break;
                ?>
                <div class="_img_left" data-aos="<?php echo $active = ($i == 1) ? 'fade-down' : 'fade-up' ;?>" style="background-image:url('<?php echo $image_src; ?>')"></div>
                <?php $i++; ?>
            <?php endforeach; ?>
        <?php endif ?>
        <div class="_group" data-aos="zoom-in"></div>
    </div>
</div>

<div class="intro_s5">
    <div class="_title_svg_m" data-aos="fade-down">
        <?php include TEMPLATE_PATH . '/assets/images/intro/s2_Vinhomes _Green _Villas.svg' ;?>
    </div>
    <div class="_title_svg" data-aos="fade-down">
        <?php include TEMPLATE_PATH . '/assets/images/intro/s3_Vinhomes _Green _Villas.svg' ;?>
    </div>
    <div class="_title_text" data-aos="fade-down"><?php echo $screen_5_title;?></div>
    <div class="_text" data-aos="fade-down"><?php echo apply_filters('the_content', $screen_5_desc); ?></div>
    <div class="_map" data-aos="fade-up">
        <?php if (isset($screen_5_image) && $screen_5_image): ?> 
            <?php foreach ($screen_5_image as $image ) : ?>
                <?php 
                $image_id = $image['id'];
                $image_src = tu_get_image_src_by_attachment_id( $image_id, 'full' );
                ?>
            <?php endforeach; ?>
        <?php endif ?>
        <img class="panzoom"src="<?php echo $image_src; ?>" alt="">
        <a href="<?php echo $image_src; ?>" class="fancybox_library"><i class="fa fa-search" aria-hidden="true"></i></a>
    </div>
</div>
<?php endwhile; ?>
<?php endif; ?> 
<?php include_once (TEMPLATE_PATH. '/partials/home/section_8.php');?>
<script>
    jQuery(document).ready(function($) {
        var swiper_intro_s3 = new Swiper('#swc_intro', {
            effect: 'coverflow',
            grabCursor: true,
            centeredSlides: true,
            slidesPerView: 'auto',
            clickable: true,
            loop:true,
            speed: 1200,
            autoplay: true,
            coverflowEffect: {
                rotate: 0,
                stretch: 0,
                depth: 250,
                modifier: 1,
                slideShadows : true,
            },
            pagination: {
                el: '#swp_intro',
                dynamicBullets: true,
            },
            navigation: {
                nextEl: '#swc_intro_next',
                prevEl: '#swc_intro_prev',
            },
        });

		var panzoom = $('.panzoom').panzoom({
			startTransform: "scale(1)",
			increment: 0.1,
			minScale: 1,
			maxScale: 4,
			contain: 'invert',
		}).panzoom('zoom');
		panzoom.parent().on('mousewheel.focal', function(e){
			e.preventDefault();
			var delta = e.delta || e.originalEvent.wheelDelta;
			var zoomOut = delta ? delta < 0 : e.originalEvent.deltaY > 0;
			panzoom.panzoom('zoom', zoomOut, {
				increment: 0.1,
				animate: false,
				focal: e
			});
		});
    });
</script>
<?php get_footer(); ?>