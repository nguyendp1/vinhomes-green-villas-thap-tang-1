<?php //  Template Name: Teaser Page ?>

<?php 
get_header(); 
$slide_teaser = tu_get_slide_teaser_with_pagination(1, -1);
$hotline = get_option('option_phone') ?: 'javascript:void(0)';
?>

<div class="teaser" style="background-image:url(<?php echo IMAGE_URL.'/teaser/teaser-bg.png'; ?>);">
	<div class="logo-teaser"><img src="<?php echo IMAGE_URL.'/teaser/teaser-logo.png'; ?>" alt=""></div>
	<div class="slide-teaser">
		<div class="swiper-container js-slide-teaser">
			<div class="swiper-wrapper">
				<?php if ( $slide_teaser->have_posts() ) : ?>
					<?php while ( $slide_teaser->have_posts() ) : $slide_teaser->the_post(); ?>
						<?php
						$post_id = get_the_ID(); 
						$teaser_title1 = get_post_meta($post_id, 'teaser_title1', true);
						$teaser_title2 = get_post_meta($post_id, 'teaser_title2', true);
						$banner = has_post_thumbnail( $post_id ) ? tu_get_post_thumbnail_src_by_post_id( $post_id, 'banner' ) : '';
						?>
						<div class="swiper-slide" style="background-image:url(<?php echo $banner;?>);">
							<div class="content-slide">
								<p><?php echo $teaser_title1;?></p>
								<p><?php echo $teaser_title2;?></p>
							</div>
						</div>
					<?php endwhile; ?>
				<?php endif; ?> 
			</div> 
			<div class="swiper-pagination pagi-default"></div>
		</div> 
	</div>
	<div class="form-teaser" style="background-image:url(<?php echo IMAGE_URL.'/teaser/teasaer-bg-form.png'; ?>);">	
		<div class="title-form">
			<img src="<?php echo IMAGE_URL.'/teaser/teaser-text.svg'; ?>" alt="">
			<h2>đăng kí nhận thông tin</h2>
		</div>	 
		<form action="#" method="post" id="form_submit" class="_form_content">
			<input type="hidden" name="action" value="submit_contact">
			<input type="hidden" name="nonce" value="<?php echo wp_create_nonce('submit_contact'); ?>">
			<div class="form-field-all">
				<div class="_form_group">
					<label for="">Họ và tên</label>
					<input type="text" class="_form_control" id="focus_name" name="contact_name" placeholder="Nguyễn Văn A">
					<div class="_error_msg _name_msg"></div>
				</div>
				<div class="_form_group">
					<label for="">Số điện thoại</label>
					<input type="tel" class="_form_control" id="focus_phone" name="contact_phone" placeholder="0912345678">
					<div class="_error_msg _phone_msg"></div>
				</div>
				<div class="_form_group">
					<label for="">Email</label>
					<input type="email" class="_form_control" id="focus_email" name="contact_email" placeholder="khongbatbuoc@gmail.com">
					<div class="_error_msg _email_msg"></div>
				</div>
				<div class="g-recaptcha" data-sitekey="6LfRoJ8UAAAAAN_EMvxdvDRcmyjHnnd5mLT8Tloq"></div>
				<div class="message"></div>
				<div class="_btn_submit">
					<button type="submit" class="lirbrary_send">đăng kí</button>
				</div>
			</div>
			<div class="frm_msg2"></div>
		</form> 
		<script src='https://www.google.com/recaptcha/api.js'></script>
		<div class="footer-form">
			<p>Hotline</p>
			<p><a href="tel:<?php echo $hotline?>"><?php echo $hotline?></a></p>
			<p><i class="fa fa-copyright"></i> Vinhomes Green Villas 2019. All right reserved </p>
		</div> 
	</div>
</div>
<style>
header, .social{
	opacity: 0;
	visibility: hidden;
} 
</style>

<script>
	jQuery(document).ready(function ($) {

		var swiper = new Swiper('.js-slide-teaser', {
			pagination: {
				el: '.js-slide-teaser .swiper-pagination',
			},
			effect: "fade",
		});

		width = $(window).width();
		if (width < 481) {
			$('.teaser .form-teaser .title-form').html("<img src='<?php echo IMAGE_URL.'/teaser/teaser-text-mb.svg'; ?>' alt=''> <h2>đăng kí nhận thông tin</h2>"); 
		}

		/*Check form s8*/
		var formButton = $('#form_submit').find('button');
		var name_msg = $('body').find('._name_msg');
		var phone_msg = $('body').find('._phone_msg');
		var email_msg = $('body').find('._email_msg');
		var check_boolean_name = false;
		var check_boolean_phone = false;
		var check_boolean_email = false;

		function check_boolean_false($name){
			formButton.css("cursor", "not-allowed");
			formButton.prop('disabled', true);
			$name = false;
		}
		function check_boolean_true($name){
			formButton.css("cursor", "pointer");
			formButton.prop('disabled', false);
			$name = true;
		}

		$('#focus_name').keyup(function (event) {
			var name = $(this).val();
			console.log(name.length);
			var regex_name = /^[a-zA-Z_àáảãạâầấẩẫậăằắẳẵặèéẻẽẹêềếểễệđìíỉĩịòóỏõọôồốổỗộơờớởỡợùúủũụưừứửữựỳýỷỹỵÀÁẢÃẠÂẦẤẨẪẬĂẰẮẲẴẶÈÉẺẼẸÊỀẾỂỄỆĐÌÍỈĨỊÒÓỎÕỌÔỒỐỔỖỘƠỜỚỞỠỢÙÚỦŨỤƯỪỨỬỮỰỲÝỶỸỴÂĂĐÔƠƯ\s-']+$/;
			if( !regex_name.test(name) ) {
				name_msg.text('Họ và tên không hợp lệ!');
				check_boolean_false(check_boolean_name);
			}else if(name.length > 35){
				name_msg.text('Họ và tên không nhập quá 35 ký tự!');
				check_boolean_false(check_boolean_name);
			}else{
				name_msg.text('');
				check_boolean_true(check_boolean_name);
			}
		});

		$('#focus_phone').keyup(function(event){
			var phone = $(this).val();
			var regex_phone = /^08([0-9]{8})|09([0-9]{8})|01([0-9]{9})|03([0-9]{8})$/;
			if( !regex_phone.test(phone) ) {
				phone_msg.text('Số điện thoại không hợp lệ!');
				check_boolean_false(check_boolean_phone);
			}else{
				phone_msg.text('');
				check_boolean_true(check_boolean_phone);
			}
		});

		$('#focus_email').keyup(function(event){
			var email = $(this).val();
			var regex_email = /^([a-z0-9_\.\-\']+\@[\da-z\.-]+\.[a-z\.]{2,3})$/;
			if( !regex_email.test(email) ) {
				email_msg.text('Email không hợp lệ!');
				check_boolean_false(check_boolean_email);
			}else if(email.length > 70){
				email_msg.text('Email không nhập quá 70 ký tự!');
				check_boolean_false(check_boolean_email);
			}
			else{
				email_msg.text('');
				check_boolean_true(check_boolean_email);
			}
		});

		/*ajax handle*/
		$("#form_submit").submit(function (event) {
			event.preventDefault();
			var thisEl = $(this);
			var formField = thisEl.find('.form-field-all');
			var thisSubmitBtn = thisEl.find('button');
			var formMsg = thisEl.find('.message');
			var formMsg2 = thisEl.find('.frm_msg2');

			$.ajax({
				url: wp_vars.ADMIN_AJAX_URL,
				type: 'post',
				dataType: 'json',
				data: thisEl.serialize(),
				beforeSend: function () {
					$(thisSubmitBtn).text("Đang gửi đi . . .");
					$(thisSubmitBtn).prop('disabled', true);
				},
				complete: function () {
					$(thisSubmitBtn).text("Đăng kí");
					$(thisSubmitBtn).prop('disabled', false);
				}
			})
			.done(function (response) {
				if (response.success) {
					formField.hide();
					thisSubmitBtn.hide();
					formMsg2.text(response.msg);
				}
				if (response.msg) {
					formMsg.text(response.msg);
				}
			})
			.fail(function () {
				console.log('ERROR');
			});
		});

	});
</script>
