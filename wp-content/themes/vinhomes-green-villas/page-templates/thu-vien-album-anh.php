<?php //  Template Name: Thư viện - Album ảnh ?>

<?php get_header(); ?>  

<div class="page-library-albums">
	<div class="page-title">
		<p>Thư viện</p>
		<div class="decor">
		</div>
		<div class="breadcrumb">
			<div class="icon">
				<i class="fa fa-home" aria-hidden="true"></i>
			</div>
			<a href="#">Trang chủ /</a>
			<a href="#"class="active">Thư viện</a>
		</div>
	</div>
	<div class="page-content">
		<div class="nav-library">
			<a href="#" class="active"> album ảnh</a>
			<a href="#" >tài liệu</a>
			<a href="#"> chính sách bán hàng</a>
		</div> 
		<div class="--albums clearfix">
			<?php for ($i = 1; $i <= 3; $i++) : ?> 
				<a href="javascript:void(0)" class="item-album" data-attr="--popup-1" style="background-image:url(<?php echo IMAGE_URL.'/library/lib_1.png'; ?>)">
					<div class="title-lib">
						<p>Album</p>
						<p>Phối cảnh tổng thể</p>
					</div>
				</a>
				<a href="javascript:void(0)" class="item-album" data-attr="--popup-2" style="background-image:url(<?php echo IMAGE_URL.'/library/lib_2.png'; ?>)">
					<div class="title-lib">
						<p>Album</p>
						<p>Phối cảnh tổng thể</p>
					</div>
				</a>
				<a href="javascript:void(0)" class="item-album" data-attr="--popup-3" style="background-image:url(<?php echo IMAGE_URL.'/library/lib_3.png'; ?>)">
					<div class="title-lib">
						<p>Album</p>
						<p>Phối cảnh tổng thể</p>
					</div>
				</a> 
			<?php endfor; ?>
		</div>  
		<a href="#" class="lirbrary_more btn_animation">xem thêm</a>
	</div>
	<?php include_once (TEMPLATE_PATH. '/partials/home/section_8.php');?>
</div>

<div class="slide-albums">
	<div class="slide is-active" id="--popup-1">
		<div class="slider-for">
			<div class="item">
				<img src="http://kembeachresort.com/wp-content/uploads/2018/12/utilities-img13.jpg" alt="" height="500px">
			</div>
			<div class="item">
				<img src="http://kembeachresort.com/wp-content/uploads/2018/12/utilities-img12.jpg" alt="" height="500px">
			</div>
			<div class="item">
				<img src="http://kembeachresort.com/wp-content/uploads/2018/12/utilities-img11.jpg" alt="" height="500px">
			</div>
			<div class="item">
				<img src="http://kembeachresort.com/wp-content/uploads/2018/12/utilities-img10.jpg" alt="" height="500px">
			</div>
			<div class="item">
				<img src="http://kembeachresort.com/wp-content/uploads/2018/12/utilities-img13.jpg" alt="" height="500px">
			</div>
		</div>

		<div class="slider-nav">
			<div class="item">
				<img src="http://kembeachresort.com/wp-content/uploads/2018/12/utilities-img13.jpg" alt="" height="500px">
			</div>
			<div class="item">
				<img src="http://kembeachresort.com/wp-content/uploads/2018/12/utilities-img12.jpg" alt="" height="500px">
			</div>
			<div class="item">
				<img src="http://kembeachresort.com/wp-content/uploads/2018/12/utilities-img13.jpg" alt="" height="500px">
			</div>
			<div class="item">
				<img src="http://kembeachresort.com/wp-content/uploads/2018/12/utilities-img10.jpg" alt="" height="500px">
			</div>
			<div class="item">
				<img src="http://kembeachresort.com/wp-content/uploads/2018/12/utilities-img13.jpg" alt="" height="500px">
			</div>
		</div>
	</div>
	<div class="slide" id="--popup-2">
		<div class="slider-for">
			<div class="item">
				<img src="http://kembeachresort.com/wp-content/uploads/2018/12/utilities-img13.jpg" alt="" height="500px">
			</div>
			<div class="item">
				<img src="http://kembeachresort.com/wp-content/uploads/2018/12/utilities-img12.jpg" alt="" height="500px">
			</div>
			<div class="item">
				<img src="http://kembeachresort.com/wp-content/uploads/2018/12/utilities-img11.jpg" alt="" height="500px">
			</div>
			<div class="item">
				<img src="http://kembeachresort.com/wp-content/uploads/2018/12/utilities-img10.jpg" alt="" height="500px">
			</div>
			<div class="item">
				<img src="http://kembeachresort.com/wp-content/uploads/2018/12/utilities-img13.jpg" alt="" height="500px">
			</div>
		</div>

		<div class="slider-nav">
			<div class="item">
				<img src="http://kembeachresort.com/wp-content/uploads/2018/12/utilities-img13.jpg" alt="" height="500px">
			</div>
			<div class="item">
				<img src="http://kembeachresort.com/wp-content/uploads/2018/12/utilities-img12.jpg" alt="" height="500px">
			</div>
			<div class="item">
				<img src="http://kembeachresort.com/wp-content/uploads/2018/12/utilities-img13.jpg" alt="" height="500px">
			</div>
			<div class="item">
				<img src="http://kembeachresort.com/wp-content/uploads/2018/12/utilities-img10.jpg" alt="" height="500px">
			</div>
			<div class="item">
				<img src="http://kembeachresort.com/wp-content/uploads/2018/12/utilities-img13.jpg" alt="" height="500px">
			</div>
		</div>
	</div>
	<div class="slide" id="--popup-3">
		<div class="slider-for">
			<div class="item">
				<img src="http://kembeachresort.com/wp-content/uploads/2018/12/utilities-img13.jpg" alt="" height="500px">
			</div>
			<div class="item">
				<img src="http://kembeachresort.com/wp-content/uploads/2018/12/utilities-img12.jpg" alt="" height="500px">
			</div>
			<div class="item">
				<img src="http://kembeachresort.com/wp-content/uploads/2018/12/utilities-img11.jpg" alt="" height="500px">
			</div>
			<div class="item">
				<img src="http://kembeachresort.com/wp-content/uploads/2018/12/utilities-img10.jpg" alt="" height="500px">
			</div>
			<div class="item">
				<img src="http://kembeachresort.com/wp-content/uploads/2018/12/utilities-img13.jpg" alt="" height="500px">
			</div>
		</div>

		<div class="slider-nav">
			<div class="item">
				<img src="http://kembeachresort.com/wp-content/uploads/2018/12/utilities-img13.jpg" alt="" height="500px">
			</div>
			<div class="item">
				<img src="http://kembeachresort.com/wp-content/uploads/2018/12/utilities-img12.jpg" alt="" height="500px">
			</div>
			<div class="item">
				<img src="http://kembeachresort.com/wp-content/uploads/2018/12/utilities-img13.jpg" alt="" height="500px">
			</div>
			<div class="item">
				<img src="http://kembeachresort.com/wp-content/uploads/2018/12/utilities-img10.jpg" alt="" height="500px">
			</div>
			<div class="item">
				<img src="http://kembeachresort.com/wp-content/uploads/2018/12/utilities-img13.jpg" alt="" height="500px">
			</div>
		</div>
	</div> 
</div>

<script>
	jQuery(document).ready(function($){
		$('.slider-for').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: false,
			fade: true,
			asNavFor: '.slider-nav',
			// autoplay: true
		});
		$('.slider-nav').slick({
			slidesToShow: 4,
			slidesToScroll: 1,
			asNavFor: '.slider-for',
			dots: false,
			centerMode: true,
			focusOnSelect: true

		});
		$('.--albums .item-album').click(function(event) {
			$('.slide').removeClass('is-active');
			var data_attr = $(this).attr('data-attr');
			var id = '#' + data_attr; 
			$(id).addClass('is-active');
		});

	});

</script> 

<?php get_footer(); ?>

<style>
	.slider-for{
		width: 500px;
		height: 250px;
		margin: 30px auto 1px;
		overflow: hidden;
	}
	.slide{
		opacity: 0;
		visibility: hidden;
		height: 0;
	}
	.slide.is-active{
		opacity: 1;
		visibility: visible;
		height: auto;
	}
	.slide img{
		width: 100%;
		min-height: 100%;
	}
	.slider-nav{
		/*width: 500px;
		height: 85px;
		margin: auto;*/
	}
	.slider-nav .slick-track{
		height: 85px;
	}
	.slick-arrow{
		position: absolute;
		top: 50%;
		z-index: 50;
		margin-top: -12px;
	}
	.slick-prev{
		left: 0;
	}
	.slick-next{
		right: 0;
	}
</style>






















