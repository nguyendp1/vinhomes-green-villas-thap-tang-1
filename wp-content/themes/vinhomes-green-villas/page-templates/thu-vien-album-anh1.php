<?php //  Template Name: Thư viện - Album ảnh1 ?>

<?php get_header(); ?>   

<div class="slider slider-for">
	<div>
		<h3>1</h3>
	</div>
	<div>
		<h3>2</h3>
	</div>
	<div>
		<h3>3</h3>
	</div>
	<div>
		<h3>4</h3>
	</div>
	<div>
		<h3>5</h3>
	</div>
</div>
<div class="slider slider-nav">
	<div>
		<h3>1</h3>
	</div>
	<div>
		<h3>2</h3>
	</div>
	<div>
		<h3>3</h3>
	</div>
	<div>
		<h3>4</h3>
	</div>
	<div>
		<h3>5</h3>
	</div>
</div>
<script>
	jQuery(document).ready(function($) {
		$('.slider-for').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: false,
			fade: true,
			asNavFor: '.slider-nav'
		});
		$('.slider-nav').slick({
			slidesToShow: 3,
			slidesToScroll: 1,
			asNavFor: '.slider-for',
			dots: true,
			centerMode: true,
			focusOnSelect: true
		});
	});
	
</script>