<?php //  Template Name: Thư viện chính sách bán hàng ?>

<?php get_header(); ?>
<div class="page-library-sales-policy">
	<div class="page-title">
		<p>Thư viện</p>
		<div class="decor">
		</div>
		<div class="breadcrumb">
			<div class="icon">
				<i class="fa fa-home" aria-hidden="true"></i>
			</div>
			<a href="#">Trang chủ /</a>
			<a href="#"class="active">Thư viện</a>
		</div>
	</div>
	<div class="page-content">
		<div class="nav-library">
			<a href="#"> album ảnh</a>
			<a href="#" >tài liệu</a>
			<a href="#" class="active"> chính sách bán hàng</a>
		</div>
		<div class="list">
			<div class="thumnail" style="background-image: url('<?php echo IMAGE_URL.'/pages/bg-library-1.png'; ?>');">
				<div class="logo-page">
					<img src="<?php echo IMAGE_URL.'/pages/logo-page.png'; ?>">
				</div>
				<div class="document">
					<p>Chính sách bán hàng T4/2019</p>
					<div class="icon">
						<div class="img"><img src="<?php echo IMAGE_URL.'/pages/eye.png'; ?>" alt=""></div>
					</div>
				</div>
			</div>
			<div class="thumnail" style="background-image: url('<?php echo IMAGE_URL.'/pages/bg-library-1.png'; ?>');">
				<div class="logo-page">
					<img src="<?php echo IMAGE_URL.'/pages/logo-page.png'; ?>">
				</div>
				<div class="document">
					<p>Chính sách bán hàng T4/2019</p>
					<div class="icon">
						<div class="img"><img src="<?php echo IMAGE_URL.'/pages/eye.png'; ?>" alt=""></div>
					</div>
				</div>
			</div>
			<div class="thumnail" style="background-image: url('<?php echo IMAGE_URL.'/pages/bg-library-1.png'; ?>');">
				<div class="logo-page">
					<img src="<?php echo IMAGE_URL.'/pages/logo-page.png'; ?>">
				</div>
				<div class="document">
					<p>Chính sách bán hàng T4/2019</p>
					<div class="icon">
						<div class="img"><img src="<?php echo IMAGE_URL.'/pages/eye.png'; ?>" alt=""></div>
					</div>
				</div>
			</div>
			<div class="thumnail" style="background-image: url('<?php echo IMAGE_URL.'/pages/bg-library-1.png'; ?>');">
				<div class="logo-page">
					<img src="<?php echo IMAGE_URL.'/pages/logo-page.png'; ?>">
				</div>
				<div class="document">
					<p>Chính sách bán hàng T4/2019</p>
					<div class="icon">
						<div class="img"><img src="<?php echo IMAGE_URL.'/pages/eye.png'; ?>" alt=""></div>
					</div>
				</div>
			</div>
			<div class="thumnail" style="background-image: url('<?php echo IMAGE_URL.'/pages/bg-library-1.png'; ?>');">
				<div class="logo-page">
					<img src="<?php echo IMAGE_URL.'/pages/logo-page.png'; ?>">
				</div>
				<div class="document">
					<p>Chính sách bán hàng T4/2019</p>
					<div class="icon">
						<div class="img"><img src="<?php echo IMAGE_URL.'/pages/eye.png'; ?>" alt=""></div>
					</div>
				</div>
			</div>
			<div class="thumnail" style="background-image: url('<?php echo IMAGE_URL.'/pages/bg-library-1.png'; ?>');">
				<div class="logo-page">
					<img src="<?php echo IMAGE_URL.'/pages/logo-page.png'; ?>">
				</div>
				<div class="document">
					<p>Chính sách bán hàng T4/2019</p>
					<div class="icon">
						<div class="img"><img src="<?php echo IMAGE_URL.'/pages/eye.png'; ?>" alt=""></div>
					</div>
				</div>
			</div>
		</div>
		<div class="link">
			<a href="#">Xem thêm</a>
		</div>
	</div>
	<?php include_once (TEMPLATE_PATH. '/partials/home/section_8.php');?>
</div>

<?php get_footer(); ?>
<script>
	jQuery(document).ready(function ($) {

	});
</script>
