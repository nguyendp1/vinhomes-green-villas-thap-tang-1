<?php //  Template Name: Tiện ích ?>
<?php get_header(); ?>
<div class="page-utilities">
	<div class="page-title">
		<p>Tiện tích</p>
		<div class="decor">
		</div>
		<div class="breadcrumb">
			<div class="icon">
				<i class="fa fa-home" aria-hidden="true"></i>
			</div>
			<a href="#">Trang chủ /</a>
			<a href="#"class="active">Tiện tích</a>
		</div>
	</div>
	<div class="page-content">
		<div class="map-utilities">
			<div class="left">
				<div class="img">
					<img src="<?php echo IMAGE_URL.'/pages/map-utilities.png'; ?>" alt="">
				</div>
			</div>
			<div class="right">
				<div class="title">
					<img src="<?php echo IMAGE_URL.'/pages/title.png'; ?>">
					<p>bản đồ tiện ích</p>
				</div>
			</div>
		</div>
		<div class="list-utitlities">
			<div class="title">
				Dánh sách tổng thể tiện ích
			</div>
			<div class="list">
				<div class="text">
					<p>1</p>
					<p> Hồ cảnh quan</p>
				</div>
				<div class="text">
					<p>2</p>
					<p> Bể bơi ngoài trời</p>
				</div>
				<div class="text">
					<p>3</p>
					<p> Khu phụ trợ bể bơi</p>
				</div>
				<div class="text">
					<p>4</p>
					<p>Sân tập dưỡng sinh</p>
				</div>
				<div class="text">
					<p>5</p>
					<p> Thảm cỏ tập dưỡng sinh</p>
				</div>
				<div class="text">
					<p>6</p>
					<p> Vườn thiền</p>
				</div>
				<div class="text">
					<p>7</p>
					<p> Vườn cờ</p>
				</div>
				<div class="text">
					<p>8</p>
					<p> Thảm cỏ đa năng</p>
				</div>
				<div class="text">
					<p>9</p>
					<p> Sân chơi trẻ em</p>
				</div>
				<div class="text">
					<p>10</p>
					<p> Sân vận động liên hoàn</p>
				</div>
				<div class="text">
					<p>11</p>
					<p> Khu BBQ</p>
				</div>
				<div class="text">
					<p>12</p>
					<p> Vườn cảnh quan</p>
				</div>
				<div class="text">
					<p>13</p>
					<p> Sân bóng chuyền mói</p>
				</div>
				<div class="text">
					<p>14</p>
					<p> Sân cầu lông</p>
				</div>
				<div class="text">
					<p>15</p>
					<p> Sân Tennis</p>
				</div>
				<div class="text">
					<p>16</p>
					<p> Sân tập bóng bàn</p>
				</div>
				<div class="text">
					<p>17</p>
					<p> Sân tập bóng rổ</p>
				</div>
				<div class="text">
					<p>18</p>
					<p> Lối vào</p>
				</div>
			</div>
		</div>
	</div>
	
	<?php include_once (TEMPLATE_PATH. '/partials/home/section_7.php');?>
	<?php include_once (TEMPLATE_PATH. '/partials/home/section_8.php');?>
</div>

<?php get_footer(); ?>
<script>
	jQuery(document).ready(function ($) {

	});
</script>
