<?php //  Template Name: Tiến Độ ?>
<?php get_header(); ?>
<div class="tiendoduan">
	<div class="page-title">
		<p>Tiến độ dự án</p>
		<div class="decor"></div>
		<div class="breadcrumb">
			<div class="icon">
				<i class="fa fa-home" aria-hidden="true"></i>
			</div>
			<a href="#">Trang chủ /</a>
			<a href="#"class="active">Tiến độ</a>
		</div>
	</div>
	<div class="the-content">
		<div class="item-block">
			<div class="img" style="background-image:url('<?php echo IMAGE_URL.'/tiendoduan/1.png'; ?>')"></div>
			<div class="text">
				<div class="date">
					12/05/2019
				</div>
				<div class="title">
					TIẾN ĐỘ DỰ ÁN VINHOMES Green villas 5/2019
				</div>
				<p class="normal">
					Dự án Vinhomes Green Villas ngay khi vừa ra mắt đã được người Hà Nội hào hứng săn đón bởi tại đây, ngoài vị trí độc tôn, ngoài tiện ích 5 sao từ hệ sinh thái … 
				</p>
				<a href="#" class="more">Xem chi tiết</a>
			</div>
		</div>
		<div class="item-block">
			<div class="img" style="background-image:url('<?php echo IMAGE_URL.'/tiendoduan/1.png'; ?>')" >	
			</div>
			<div class="text">
				<div class="date">
					12/05/2019
				</div>
				<div class="title">
					TIẾN ĐỘ DỰ ÁN VINHOMES Green villas 5/2019
				</div>
				<p class="normal">
					Dự án Vinhomes Green Villas ngay khi vừa ra mắt đã được người Hà Nội hào hứng săn đón bởi tại đây, ngoài vị trí độc tôn, ngoài tiện ích 5 sao từ hệ sinh thái … 
				</p>
				<a href="#" class="more">Xem chi tiết</a>
			</div>
		</div>
		<div class="item-block">
			<div class="img" style="background-image:url('<?php echo IMAGE_URL.'/tiendoduan/1.png'; ?>')">	
			</div>
			<div class="text">
				<div class="date">
					12/05/2019
				</div>
				<div class="title">
					TIẾN ĐỘ DỰ ÁN VINHOMES Green villas 5/2019
				</div>
				<p class="normal">
					Dự án Vinhomes Green Villas ngay khi vừa ra mắt đã được người Hà Nội hào hứng săn đón bởi tại đây, ngoài vị trí độc tôn, ngoài tiện ích 5 sao từ hệ sinh thái … 
				</p>
				<a href="#" class="more">Xem chi tiết</a>
			</div>
		</div>
		<div class="link">
			<a href="#" class="lirbrary_more btn_animation">xem thêm</a>
		</div>
	</div>
</div>
<?php include_once (TEMPLATE_PATH. '/partials/home/section_8.php');?>
<?php get_footer(); ?>