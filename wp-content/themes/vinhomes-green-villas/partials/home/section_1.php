<?php
$slide_banner = tu_get_slide_banner_with_pagination(1, -1);
?>
<section class="_1"> 
  <div class="swiper-container js-slide-s1">
    <div class="swiper-wrapper">
      <?php if ( $slide_banner->have_posts() ) : ?>
        <?php while ( $slide_banner->have_posts() ) : $slide_banner->the_post(); ?>
          <?php
          $slide_id = get_the_ID(); 
          $banner_title1 = get_post_meta($slide_id, 'banner_title1', true);
          $banner_title2 = get_post_meta($slide_id, 'banner_title2', true);
          $banner = has_post_thumbnail( $slide_id ) ? tu_get_post_thumbnail_src_by_post_id( $slide_id, 'slide_banner' ) : '';
          ?>
          <div class="swiper-slide" >
            <div class="background" style="background-image:url(<?php echo $banner;?>);"></div>
            <div class="title-s1">
              <p data-aos="fade-right" data-aos-duration="2000" data-aos-anchor="._1"><?php echo $banner_title1;?></p>
              <p data-aos="fade-right" data-aos-duration="3000" data-aos-delay="1000" data-aos-anchor="._1"><?php echo $banner_title2;?></p>
            </div>
          </div> 
        <?php endwhile; ?>
      <?php endif; ?>
    </div> 
    <div class="swiper-pagination pagi-default"></div>
  </div>
</section>


<div class="scroll" id="js-scrolldown">
  <p>scroll down </p>
  <img src="<?php echo IMAGE_URL.'/home/s123/scroll.svg'; ?>" alt="">
</div>

<script>
  jQuery(document).ready(function($) {
    var swiper = new Swiper('.js-slide-s1', {
      pagination: {
        el: '.js-slide-s1 .swiper-pagination',
        clickable: true,
          dynamicBullets: true,
      },
      effect: "fade",
      loop: true
    });
    $('#js-scrolldown').click(function (event) {
      event.preventDefault();
      $('body, html').animate({
        scrollTop: $(window).height()
      }, 700);
    });
  });

</script>