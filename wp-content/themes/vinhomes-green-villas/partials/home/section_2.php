<section class="_2">
	<div class="background"> 
		<?php include_once (TEMPLATE_PATH.'/assets/images/home/s123/s2-text.svg'); ?> 
		<img src="<?php echo IMAGE_URL.'/home/s123/s2-bg.png'; ?>" alt="" class="leaf">
	</div>
	<div class="content">
		<div class="text"  data-aos="fade-left" data-aos-duration="2000" data-aos-anchor="._2">
			<p><?php echo $screen_2_label;?></p>
			<?php echo apply_filters('the_content', $screen_2_desc); ?>
		</div>
		<div class="images" data-aos="flip-right" data-aos-duration="2000" data-aos-anchor="._2">
			<?php
			$screen_2_image = get_post_meta($post_id, 'screen_2_image', true);
			$image_src_default = IMAGE_URL.'/home/s3.png';
			?>
			<?php if ( isset( $screen_2_image ) && $screen_2_image ) : ?>
				<?php foreach ( $screen_2_image as $item) : ?>
					<?php
					$img_id = $item['id'];
					$img_src = tu_get_image_src_by_attachment_id($img_id,'screen_2_image');
					$img_src_used = $img_src ?: $image_src_default;
					?>
				<?php endforeach; ?>
			<?php endif; ?>
			<img src="<?php echo $img_src_used;?>" alt="">
		</div>
	</div>
</section>