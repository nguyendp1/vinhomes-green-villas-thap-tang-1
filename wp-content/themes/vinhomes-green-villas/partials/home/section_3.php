<div class="section_3">
	<?php
		$screen_3_image = get_post_meta($post_id, 'screen_3_image', true);
		$image_src_default = IMAGE_URL.'/home/s3.png';
		?>
		<?php if ( isset( $screen_3_image ) && $screen_3_image ) : ?>
			<?php foreach ( $screen_3_image as $item) : ?>
				<?php
				$img_id = $item['id'];
				$img_src = tu_get_image_src_by_attachment_id($img_id,'screen_3_image');
				$img_src_used = $img_src ?: $image_src_default;
				?>
			<?php endforeach; ?>
		<?php endif; ?> 
	<div class="left"  data-aos="fade-right" data-aos-duration="2000" data-aos-anchor=".section_3" style="background-image:url(<?php echo $img_src_used;?>);"> 
	</div>
	<div class="right"  data-aos="fade-left" data-aos-duration="2000" data-aos-anchor=".section_3">
		<div class="title">
			<?php include_once TEMPLATE_PATH . '/assets/images/title.svg' ;?>
		</div>
		<div class="desr"><?php echo $screen_3_label;?></div>
		<div class="text"><?php echo apply_filters('the_content', $screen_3_desc); ?></div>
	</div>
	<div class="decor">
		<img src="<?php echo IMAGE_URL.'/home/s3_decor.png'; ?>" alt="">
	</div>
</div>
