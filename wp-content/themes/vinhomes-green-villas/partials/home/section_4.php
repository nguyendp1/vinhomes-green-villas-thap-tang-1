<div class="section_4">
	<?php
	$screen_4_image = get_post_meta($post_id, 'screen_4_image', true);
	$image_src_default = IMAGE_URL.'/home/s4.png';
	?>
	<?php if ( isset( $screen_4_image ) && $screen_4_image ) : ?>
		<?php foreach ( $screen_4_image as $item) : ?>
			<?php
			$img_id = $item['id'];
			$img_src = tu_get_image_src_by_attachment_id($img_id,'screen_4_image');
			$img_src_used = $img_src ?: $image_src_default;
			?>
		<?php endforeach; ?>
	<?php endif; ?>
	<div class="left" data-aos="fade-left" data-aos-duration="2000" data-aos-anchor=".section_4" style="background-image:url(<?php echo $img_src_used;?>);"> 
	</div>
	<div class="right" data-aos="fade-right" data-aos-duration="2000" data-aos-anchor=".section_4">
		<div class="title">
			<?php include TEMPLATE_PATH . '/assets/images/title_1.svg' ;?>
		</div>
		<div class="desr"><?php echo $screen_4_label;?></div>
		<div class="text"><?php echo apply_filters('the_content', $screen_4_desc); ?></div>
	</div>
	<div class="decor">
		<img src="<?php echo IMAGE_URL.'/home/s4_decor.png'; ?>" alt="">
	</div>
</div>