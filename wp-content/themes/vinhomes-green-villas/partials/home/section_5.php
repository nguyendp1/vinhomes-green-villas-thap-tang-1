<div class="section_5">
	<div class="left">
		<div class="swiper-container" id="js_s5"  data-aos="fade-right" data-aos-duration="2000" data-aos-anchor=".section_5" >
			<div class="swiper-wrapper">
				<?php if (isset($screen_5_image) && $screen_5_image): ?> 
					<?php foreach ($screen_5_image as $image ) : ?>
						<?php 
						$image_id = $image['id'];
						$image_src = tu_get_image_src_by_attachment_id( $image_id, 'full' );
						?>
						<div class="swiper-slide" style="background-image: url('<?php echo $image_src;?>')"></div>
					<?php endforeach; ?>
				<?php endif ?>
				
			</div>
		</div>
		<div class="swiper-button-next">
			<img src="<?php echo IMAGE_URL.'/home/next.png'; ?>" alt="">
		</div>
		<div class="swiper-button-prev">
			<img src="<?php echo IMAGE_URL.'/home/prev.png'; ?>" alt="">
		</div>
		<div class="swiper-pagination pagi-default"></div>
	</div>
	<div class="right"  data-aos="fade-left" data-aos-duration="2000" data-aos-anchor=".section_5">
		<div class="title">
			<?php include TEMPLATE_PATH . '/assets/images/title_1.svg' ;?>
		</div>
		<div class="desr"><?php echo $screen_5_label;?></div>
		<div class="text"><?php echo apply_filters('the_content', $screen_5_desc); ?></div>
		<div class="link btn_animation">
			<a href="<?php echo $screen_5_link;?>">xem thêm</a> 
		</div>
	</div>
</div>
<script>
	jQuery(document).ready(function($){

		var swiper = new Swiper('#js_s5', {
			navigation: {
				nextEl: '.section_5 .swiper-button-next',
				prevEl: '.section_5 .swiper-button-prev',
			},
			pagination: {
				el: '.section_5 .swiper-pagination',
			},
		});
	});
</script>