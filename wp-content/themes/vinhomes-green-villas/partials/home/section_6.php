<div class="section_6">
    <div class="_content" data-aos="fade-right">
        <div class="_title"><?php echo $screen_6_label;?></div>
        <div class="_text">
            <?php echo apply_filters('the_content', $screen_6_desc); ?>
        </div>
    </div>
    <div id="img_panzoom" class="_img_map" data-aos="fade-left" data-aos-duration="1200">
        <div class="panzoom">
            <?php
            $screen_6_image = get_post_meta($post_id, 'screen_6_image', true);
            $image_src_default = IMAGE_URL.'/home/s3.png';
            ?>
            <?php if ( isset( $screen_6_image ) && $screen_6_image ) : ?>
                <?php foreach ( $screen_6_image as $item) : ?>
                    <?php
                    $img_id = $item['id'];
                    $img_src = tu_get_image_src_by_attachment_id($img_id,'screen_6_image');
                    $img_src_used = $img_src ?: $image_src_default;
                    ?>
                <?php endforeach; ?>
            <?php endif; ?>
            <img src="<?php echo $img_src_used;?>" alt="">
        </div>
        <div class="_button_zoom">
            <div class="_zoom_out _zoom_control"><i class="fa fa-minus" aria-hidden="true"></i></div>
            <div class="_zoom_in _zoom_control"><i class="fa fa-plus" aria-hidden="true"></i></div>
        </div>
    </div>
</div>
