<div class="section_7">
    <div class="_slide_Library" data-aos="fade-up">
        <div class="swiper-container" id="swc_s7">
            <div class="swiper-wrapper"> 
                <?php if (isset($screen_7_image) && $screen_7_image): $i = 0; $a = 0;?> 
                    <?php $screen_7_image_length = count($screen_7_image); ?>

                    <?php foreach ($screen_7_image as $key => $image ) : ?>

                        <?php
                        $image_id = $image['id'];
                        $image_src = tu_get_image_src_by_attachment_id( $image_id, 'full' );

                        if($i == 0) echo "<div class='swiper-slide'>";
                        $i++;
                        $a++;

                        ?>

                        <a href="<?php echo $image_src;?>" class="fancybox_library" style="outline:none">
                            <div class="_slide_item" style="background-image:url('<?php echo $image_src;?>')"></div>
                        </a>

                        <?php 
                        if($i == 5 || $a == $screen_7_image_length) {
                            echo "</div>";
                            $i = 0;
                        }
                        ?>

                    <?php endforeach; ?>
                <?php endif ?>
            </div>
            <div class="swiper-pagination pagi-default" id="swp_s7"></div>
        </div>
    </div>
    <div class="_slide_library_mobile" data-aos="fade-up" data-aos-duration="1200">
        <div class="swiper-container" id="swc_s7_m">
            <div class="swiper-wrapper">
                <?php if (isset($screen_7_image) && $screen_7_image): $i = 0; $a = 0;?> 
                    <?php $screen_7_image_length = count($screen_7_image); ?>

                    <?php foreach ($screen_7_image as $key => $image ) : ?>

                        <?php
                        $image_id = $image['id'];
                        $image_src = tu_get_image_src_by_attachment_id( $image_id, 'full' );

                        if($i == 0) echo "<div class='swiper-slide'>";
                        $i++;
                        $a++;

                        ?>

                        <a href="<?php echo $image_src;?>" class="fancybox_library" style="outline:none">
                            <div class="_slide_item" style="background-image:url('<?php echo $image_src;?>')"></div>
                        </a>

                        <?php 
                        if($i == 3 || $a == $screen_7_image_length) {
                            echo "</div>";
                            $i = 0;
                        }
                        ?>

                    <?php endforeach; ?>
                <?php endif ?>
            </div>
        </div>
        <div class="swiper-pagination pagi-default" id="swp_s7_m"></div>
    </div>
    <div class="_button_library btn_animation"><a href="<?php echo $screen_7_link;?>">xem toàn bộ thư viện</a></div>
</div>
