
<div class="section_8">
  <div class="_img_bg" data-aos="fade-right" style="background-image:url('<?php echo IMAGE_URL.'/home/s8_img.png'; ?>')">
  </div>
  <div class="_s8_form" id="s8_form">
    <div class="form_title">
      <div class="title _title_mobile">
        <?php include TEMPLATE_PATH . '/assets/images/home/s8_title_m.svg' ;?>
      </div>
      <div class="title _title_ipad">
        <?php include TEMPLATE_PATH . '/assets/images/home/s8_title_1.svg' ;?>
      </div>
      <div class="title _img_title">
        <?php include TEMPLATE_PATH . '/assets/images/home/s8_title.svg' ;?>
      </div>
      <div class="_title">đăng kí nhận thông tin dự án</div>
      <div class="_text">Quý khách hàng quan tâm tới dự án xin vui lòng để lại thông tin để Bộ phận KD sẽ sớm phản hồi cho Quý khách!</div>
    </div>
    
    <form action="#" method="POST" id="form_submit" class="_form_content">
      <input type="hidden" name="action" value="submit_contact">
      <input type="hidden" name="nonce" value="<?php echo wp_create_nonce('submit_contact'); ?>">
      <div class="form-field-all">
        <div class="_form_group">
          <label for="">họ và tên</label>
          <input type="text" class="_form_control" id="focus_name" name="contact_name" placeholder="Nguyễn Văn A">
          <div class="_error_msg _name_msg"></div>
        </div>
        <div class="_form_group">
          <label for="">số điện thoại</label>
          <input type="text" class="_form_control" id="focus_phone" name="contact_phone" placeholder="0912345678">
          <div class="_error_msg _phone_msg"></div>
        </div>
        <div class="_form_group">
          <label for="">email</label>
          <input type="text" class="_form_control" id="focus_email" name="contact_email" placeholder="khongbatbuoc@gmail.com">
          <div class="_error_msg _email_msg"></div>
        </div>
        <div class="g-recaptcha" data-sitekey="6LdNZ6QUAAAAAFkUnM_1cXh_xlCwJ4ARL3eTiGPj"></div>
        <div class="message"></div>
        <div class="_btn_submit">
          <button type="submit" class="lirbrary_send btn_animation">đăng kí</button>
        </div>
      </div>
      <div class="frm_msg2"></div>
    </form>
    <script src='https://www.google.com/recaptcha/api.js'></script>
  </div>
  <div class="_group" data-aos="zoom-in"></div>
</div>
<script>
  jQuery(document).ready(function ($) {

    /*Check form s8*/
    var formButton = $('#form_submit').find('button');
    var name_msg = $('body').find('._name_msg');
    var phone_msg = $('body').find('._phone_msg');
    var email_msg = $('body').find('._email_msg');
    var check_boolean_name = false;
    var check_boolean_phone = false;
    var check_boolean_email = false;

    function check_boolean_false($name){
      formButton.css("cursor", "not-allowed");
      formButton.prop('disabled', true);
      $name = false;
    }
    function check_boolean_true($name){
      formButton.css("cursor", "pointer");
      formButton.prop('disabled', false);
      $name = true;
    }

    $('#focus_name').keyup(function (event) {
      var name = $(this).val();
      console.log(name.length);
      var regex_name = /^[a-zA-Z_àáảãạâầấẩẫậăằắẳẵặèéẻẽẹêềếểễệđìíỉĩịòóỏõọôồốổỗộơờớởỡợùúủũụưừứửữựỳýỷỹỵÀÁẢÃẠÂẦẤẨẪẬĂẰẮẲẴẶÈÉẺẼẸÊỀẾỂỄỆĐÌÍỈĨỊÒÓỎÕỌÔỒỐỔỖỘƠỜỚỞỠỢÙÚỦŨỤƯỪỨỬỮỰỲÝỶỸỴÂĂĐÔƠƯ\s-']+$/;
      if( !regex_name.test(name) ) {
        name_msg.text('Họ và tên không hợp lệ!');
        check_boolean_false(check_boolean_name);
      }else if(name.length > 35){
        name_msg.text('Họ và tên không nhập quá 35 ký tự!');
        check_boolean_false(check_boolean_name);
      }else{
        name_msg.text('');
        check_boolean_true(check_boolean_name);
      }
    });

    $('#focus_phone').keyup(function(event){
      var phone = $(this).val();
      var regex_phone = /^08([0-9]{8})|09([0-9]{8})|01([0-9]{9})|03([0-9]{8})$/;
      if( !regex_phone.test(phone) ) {
        phone_msg.text('Số điện thoại không hợp lệ!');
        check_boolean_false(check_boolean_phone);
      }else{
        phone_msg.text('');
        check_boolean_true(check_boolean_phone);
      }
    });

    $('#focus_email').keyup(function(event){
      var email = $(this).val();
      var regex_email = /^([a-z0-9_\.\-\']+\@[\da-z\.-]+\.[a-z\.]{2,3})$/;
      if( !regex_email.test(email) ) {
        email_msg.text('Email không hợp lệ!');
        check_boolean_false(check_boolean_email);
      }else if(email.length > 70){
        email_msg.text('Email không nhập quá 70 ký tự!');
        check_boolean_false(check_boolean_email);
      }
      else{
        email_msg.text('');
        check_boolean_true(check_boolean_email);
      }
    });

    /*ajax handle*/
    $("#form_submit").submit(function (event) {
      event.preventDefault();
      var thisEl = $(this);
      var formField = thisEl.find('.form-field-all');
      var thisSubmitBtn = thisEl.find('button');
      var formMsg = thisEl.find('.message');
      var formMsg2 = thisEl.find('.frm_msg2');

      $.ajax({
        url: wp_vars.ADMIN_AJAX_URL,
        type: 'post',
        dataType: 'json',
        data: thisEl.serialize(),
        beforeSend: function () {
          $(thisSubmitBtn).text("Đang gửi đi . . .");
          $(thisSubmitBtn).prop('disabled', true);
        },
        complete: function () {
          $(thisSubmitBtn).text("Đăng kí");
          $(thisSubmitBtn).prop('disabled', false);
        }
      })
      .done(function (response) {
        if (response.success) {
          formField.hide();
          thisSubmitBtn.hide();
          formMsg2.text(response.msg);
          $(document.body).trigger('gtm_event_tracking', ['ContactForm', 'Submit', 'Ok']);
          window.location.href = "https://greenvillas.vinhomes.vn" + '#thank_you';
        }
        if (response.msg) {
          formMsg.text(response.msg);
        }
      })
      .fail(function () {
        console.log('ERROR');
      });
    });

  });
</script>
