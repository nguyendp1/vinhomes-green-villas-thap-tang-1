<div class="section_8_1" data-aos="fade-up">
    <div class="_bg" style="background-image:url('<?php echo IMAGE_URL.'/home/s8_1_bg.png'; ?>')"></div>
    <div class="_content" data-aos="fade-right" data-aos-duration="2000" data-aos-anchor=".section_8_1">
        <div class="title_m">
           <?php include TEMPLATE_PATH . '/assets/images/home/s8_1_title_m.svg' ;?>
       </div>
       <div class="title">
           <?php include TEMPLATE_PATH . '/assets/images/home/s8_1_title.svg' ;?>
       </div>
       <div class="_title"><?php echo $screen_8_label;?></div>
       <div class="_text">
            <p><?php echo $screen_8_desc;?></p>
            <p><a href="tel:<?php echo $screen_8_hotline;?>">Hotline đại lý: <?php echo $screen_8_hotline;?> </a> </p>
        </div>
    </div>
<div class="_download">
    <div class="_document">Tài liệu dự án</div>
    <div class="_select_download">
        <div class="_input_text">
            <span></span>
        </div>
        <ul class="_text_list">
            <?php
            $document = tu_get_document_with_pagination(1, -1);
            ?>
            <?php if ( $document->have_posts() ) : $i=0;?>
                <?php while ( $document->have_posts() ) : $document->the_post(); ?>
                    <?php
                    $i++;
                    $document_id = get_the_ID(); 
                    $title = get_the_title($document_id);
                    $file_url = tu_get_document_url_by_id( $document_id, 'document_file_id' );
                    $file_url_using = $file_url ?: 'javascript:void(0)';
                    ?>
                    <li class="_text_item <?php echo $classSelected = $i == 1 ? 'selected': null; ?>" data-link="<?php echo $file_url_using;?>"><?php echo $title;?></li>
                <?php endwhile; ?>
            <?php endif; ?>
        </ul>
        <a href="" class="_button_download btn_animation" data-event="auto" data-event-category="Download" data-event-action="click" data-event-label="" download>Tải về</a> 
    </div>
    <div class="_comment"><?php echo apply_filters('the_content', $screen_8_note); ?></div>
</div>
</div>