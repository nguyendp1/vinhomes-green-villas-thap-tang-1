<?php
/**
 * TODO:
 * Chức năng export ra Excel
 */

/**
 * INITIALIZE ----------- ----------- -----------
 */
add_action('init', 'tu_reg_post_type_contact');
function tu_reg_post_type_contact() {

    //Change this when creating post type
    $post_type_name = __('Liên hệ', TEXT_DOMAIN);
    $post_type_name_lower = mb_strtolower($post_type_name, 'utf-8');
    $post_type_menu_position = 3;

    $labels = array(
        'name' => $post_type_name,
        'singular_name' => $post_type_name,
        'menu_name' => $post_type_name,
        'all_items' => __('Tất cả', TEXT_DOMAIN).' '.$post_type_name_lower,
        'add_new' => __('Thêm mới', TEXT_DOMAIN),
        'add_new_item' => __('Thêm mới', TEXT_DOMAIN).' '.$post_type_name_lower,
        'edit_item' => __('Chỉnh sửa', TEXT_DOMAIN).' '.$post_type_name_lower,
        'new_item' => $post_type_name,
        'view_item' => __('Xem chi tiết', TEXT_DOMAIN),
        'search_items' => __('Tìm kiếm', TEXT_DOMAIN),
        'not_found' => __('Không tìm thấy bản ghi nào', TEXT_DOMAIN),
        'not_found_in_trash' => __('Không có bản ghi nào trong thùng rác', TEXT_DOMAIN),
        'view' => __('Xem', TEXT_DOMAIN).' '.$post_type_name_lower,
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'show_in_nav_menus' => false,
        'show_ui' => true,

        //Change this when creating post type
        'description' => $post_type_name,
        'menu_position' => $post_type_menu_position,
        'menu_icon' => 'dashicons-feedback',
        'supports' => array('title'),
        'rewrite' => null,
        'has_archive' => false
    );

    register_post_type('contact', $args);
}

/**
 * RETRIEVING FUNCTIONS ----------- ----------- -----------
 */

/**
 * Get contacts
 *
 * @param int   $page
 * @param int   $post_per_page
 * @param array $custom_args
 *
 * @return WP_Query
 */
function tu_get_contact_with_pagination($page = 1, $post_per_page = 10, $custom_args = array()) {

    $args = array(
        'post_type' => 'contact',
        'posts_per_page' => $post_per_page,
        'paged' => $page,
        'post_status' => 'pending',
        'tax_query' => array()
    );

    $args = array_merge($args, $custom_args);

    $posts = new WP_Query($args);

    return $posts;
}

/**
 * POST META BOXES ----------- ----------- -----------
 */
add_action('admin_init', 'tu_add_meta_box_contact');
function tu_add_meta_box_contact() {

    /** Meta box for general information */
    function tu_display_meta_box_contact_general($post) {
        $post_id = $post->ID;
        $contact_name = get_post_meta($post_id, 'contact_name', true);
        $contact_phone = get_post_meta($post_id, 'contact_phone', true);
        $contact_email = get_post_meta($post_id, 'contact_email', true);
        ?>
        <table class="form-table">
            <input type="hidden" name="nonce" value="<?php echo wp_create_nonce('save_metabox_contact'); ?>">
            <tbody>
                <tr>
                    <th scope="row"><label><?php _e('Họ tên', TEXT_DOMAIN); ?></label></th>
                    <td><?php echo $contact_name; ?></td>
                </tr>
                <tr>
                    <th scope="row"><label><?php _e('SĐT', TEXT_DOMAIN); ?></label></th>
                    <td><?php echo $contact_phone; ?></td>
                </tr>
                <tr>
                    <th scope="row"><label><?php _e('Email', TEXT_DOMAIN); ?></label></th>
                    <td><?php echo $contact_email; ?></td>
                </tr>
            </tbody>
        </table>
        <?php
    }

    add_meta_box(
        'tu_display_meta_box_contact_general', __('Thông tin liên hệ', TEXT_DOMAIN), 'tu_display_meta_box_contact_general', 'contact', 'normal', 'high'
    );
}


/**
 * Contact submit Ajax
 */
add_action('wp_ajax_submit_contact', 'submit_contact');
add_action('wp_ajax_nopriv_submit_contact', 'submit_contact');
function submit_contact(){
    // Function response - boolen
    function result_data($bool, $msg) {
        echo json_encode(array('success' => $bool, 'msg' => $msg));
        exit;
    }
    // Verify nonce
    if (!isset($_POST['nonce']) || !wp_verify_nonce($_POST['nonce'], 'submit_contact')) {
        $msg = __('Phiên làm việc đã hết, vui lòng tải lại trang và thử lại.', TEXT_DOMAIN);
        result_data(false, $msg);
    }
     // Query post has metabox value exists
    function query_post_has_metabox_exists($post_type, $metabox_key, $value, $message)
    {
        $args = array(
            'post_type' => 'contact',
            'order' => 'DESC',
            'orderby' => 'ID',
            'posts_per_page' => 1,
            'paged' => 1,
            'post_status' => 'pending',
            'meta_query' => array(
                'relation' => 'OR',
                array(
                    'key' => $metabox_key,
                    'value' => $value,
                    'compare' => '='
                )
            )
        );

        $posts = new WP_Query($args);

        if ($posts->have_posts()) {
            return $message;
        }

        return false;
    }
    
    // Function query data exists
    function ajax_form_field_exists($post_type, $metabox, $value, $message)
    {
        $is_field_exists = query_post_has_metabox_exists(
            $post_type,
            $metabox,
            $value,
            $message
        );

        if (!$is_field_exists == false) {
            result_data(false, $is_field_exists);
        }
    }

    $data = array();
    $data['contact_name'] = (isset($_POST['contact_name'])) ? sanitize_text_field($_POST['contact_name']) : null;
    $data['contact_phone'] = (isset($_POST['contact_phone'])) ? sanitize_text_field($_POST['contact_phone']) : null;
    $data['contact_email'] = (isset($_POST['contact_email'])) ? sanitize_text_field($_POST['contact_email']) : null;
    $data['g-recaptcha-response'] = (isset($_POST['g-recaptcha-response'])) ? sanitize_text_field($_POST['g-recaptcha-response']) : null;
    $requires = ['contact_phone' , 'contact_name', 'contact_email', 'g-recaptcha-response'];

    // Check fields empty
    foreach ($requires as $item) {
        if (empty($data[$item])) {
            $msg = __('Vui lòng nhập đầy đủ thông tin.', TEXT_DOMAIN);
            result_data(false, $msg);
        }
    }

    // Check name
    if (!empty($data['contact_name'])) {
        if (!preg_match("/^[a-zA-Z_àáảãạâầấẩẫậăằắẳẵặèéẻẽẹêềếểễệđìíỉĩịòóỏõọôồốổỗộơờớởỡợùúủũụưừứửữựỳýỷỹỵÀÁẢÃẠÂẦẤẨẪẬĂẰẮẲẴẶÈÉẺẼẸÊỀẾỂỄỆĐÌÍỈĨỊÒÓỎÕỌÔỒỐỔỖỘƠỜỚỞỠỢÙÚỦŨỤƯỪỨỬỮỰỲÝỶỸỴÂĂĐÔƠƯ\s-']+$/", $data['contact_name'])) {
            $msg = __('Họ và tên không hợp lệ!', TEXT_DOMAIN);
            result_data(false, $msg);
        }else if(mb_strlen($data['contact_name']) > 35){
            $msg = __('Họ và tên không nhập quá 35 ký tự!', TEXT_DOMAIN);
            result_data(false, $msg);
        }
    }

    // Check phone
    if (!empty($data['contact_phone'])) {
        if (!preg_match('/^08([0-9]{8})|09([0-9]{8})|01([0-9]{9})|03([0-9]{8})$/', $data['contact_phone'])) {
            $msg = __('Số điện thoại không hợp lệ!', TEXT_DOMAIN);
            result_data(false, $msg);
        }
        ajax_form_field_exists(
            'contact',
            'contact_phone',
            $data['contact_phone'],
            __('Số điện thoại đã được đăng ký!', TEXT_DOMAIN)
        );
    }
    // Check email
    if (!empty($data['contact_email'])) {
        if (!preg_match('/^([a-z0-9_\.\-\']+\@[\da-z\.-]+\.[a-z\.]{2,3})$/',$data['contact_email'])) {
            $msg = __('Email không hợp lệ!', TEXT_DOMAIN);
            result_data(false, $msg);
        }else if(mb_strlen($data['contact_email']) > 70){
            $msg = __('Email không nhập quá 70 ký tự!', TEXT_DOMAIN);
            result_data(false, $msg);
        }
        ajax_form_field_exists(
            'contact',
            'contact_email',
            $data['contact_email'],
            __('Email đã được đăng ký!', TEXT_DOMAIN)
        );
    }

    $new_contact_agrs = array(
        'post_title' => $data['contact_name'] . ' - ' . $data['contact_phone'],
        'post_status' => 'pending',
        'post_type' => 'contact'
    );
    $new_contact = wp_insert_post($new_contact_agrs);

    if (!is_wp_error($new_contact)) {
        // Save metabox
        if (!empty($data['contact_name'])) {
            update_post_meta($new_contact, 'contact_name', $data['contact_name']);
        }
        
        if (!empty($data['contact_phone'])) {
            update_post_meta($new_contact, 'contact_phone', $data['contact_phone']);
        } 
        
        if (!empty($data['contact_email'])) {
            update_post_meta($new_contact, 'contact_email', $data['contact_email']);
        }

        $name = $data['contact_name'];
        $phone = $data['contact_phone'];
        $email = $data['contact_email'];
        // $send_mail = array('hoaithuongnt.hd@gmail.com');
        
        // $mail_content = "
        // <p><b>Thông tin khách hàng:</b></p>
        // <p>- Họ tên: $name </p>
        // <p>- Email: $email </p>
        // <p>- SĐT: $phone</p>
        // ";

        // wp_mail($email, "Vinhomes Green Villas trân trọng cảm ơn Quý khách hàng đã quan tâm đến dự án");
        // wp_mail($send_mail, "Có khách hàng mới liên hệ trên website Vinhomes Green Villas:", $mail_content );

        $msg = __('Thông tin đăng ký đã được gửi đi, chúng tôi sẽ liên hệ với bạn trong thời gian sớm nhất.', TEXT_DOMAIN);
        result_data(true, $msg);

        exit;
    } else {
        $msg = __('Có lỗi xảy ra vui lòng thử lại.', TEXT_DOMAIN);
        result_data(false, $msg);
    }

    exit;

}

/**
 * EXPORTING
 */
add_action('admin_menu', 'tu_add_contact_sub_menu_pages_export_contact');
function tu_add_contact_sub_menu_pages_export_contact()
{
    add_submenu_page(
        'edit.php?post_type=contact',
        __('Trích xuất', TEXT_DOMAIN) . ' ' . __('liên hệ', TEXT_DOMAIN),
        __('Trích xuất', TEXT_DOMAIN) . ' ' . __('liên hệ', TEXT_DOMAIN),
        'edit_posts',
        'contact_export',
        'tu_add_contact_sub_menu_pages_export_contact_callback'
    );
}
function tu_add_contact_sub_menu_pages_export_contact_callback() {
    ?>
    <div class="wrap">

        <h1><?php echo __('Trích xuất', TEXT_DOMAIN).' '.__('thông tin', TEXT_DOMAIN).' '.__('liên hệ', TEXT_DOMAIN); ?></h1>

        <form method="post" action="">

            <input type="hidden" name="nonce" value="<?php echo wp_create_nonce('export_contact'); ?>" />
            <table class="form-table">
                <tbody>

                </tbody>
            </table>

            <p class="submit"><input type="submit" name="submit" id="submit" class="button button-primary" value="<?php _e('Trích xuất', TEXT_DOMAIN); ?>"></p>

        </form>

    </div>
    <?php
}
add_action('admin_init', 'add_contact_sub_menu_pages_export_contact_handle');
function add_contact_sub_menu_pages_export_contact_handle() {
    require_once(TEMPLATE_PATH.'/includes/libraries/PHPExcel/PHPExcel/IOFactory.php');

    if(isset($_POST)){

        if(isset($_POST['nonce']) && wp_verify_nonce($_POST['nonce'], 'export_contact')){

            $contact = tu_get_contact_with_pagination(1, -1);

            $file_name = 'GreenVillas-contacts-'.date('d-m-Y');

            $objPHPExcel = new PHPExcel();
            $objPHPExcel->getActiveSheet()->setTitle($file_name);
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'STT')
                ->setCellValue('B1', 'Họ tên')
                ->setCellValue('C1', 'Số điện thoại')
                ->setCellValue('D1', 'Email')
                ->setCellValue('E1', 'Tình trạng phản hồi')
                ->setCellValue('F1', 'Ngày đăng ký');

            $stt = 0;
            $row = 1;
            while($contact->have_posts()):

                $contact->the_post();
                $stt++;
                $row++;

                $post_id = get_the_ID();
                $contact_name = get_post_meta($post_id, 'contact_name', true);
                $contact_phone = get_post_meta($post_id, 'contact_phone', true);
                $contact_email = get_post_meta($post_id, 'contact_email', true);
                $contact_status = get_post_status($post_id);
                $contact_date = get_the_date('d/m/Y', $post_id);


                $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $row, $stt)
                    ->setCellValue('B' . $row, $contact_name)
                    ->setCellValue('C' . $row, $contact_phone)
                    ->setCellValue('D' . $row, $contact_email)
                    ->setCellValue('E' . $row, $contact_status)
                    ->setCellValue('F' . $row, $contact_date);

            endwhile;

            ob_end_clean();
            ob_start();

            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="'.$file_name.'.xls"');
            header('Cache-Control: max-age=0');

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

            $objWriter->save('php://output'); exit;
        }
    }
}