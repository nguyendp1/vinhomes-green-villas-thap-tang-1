<?php
/**
 * INITIALIZE ----------- ----------- -----------
 */

add_action('init', 'tu_reg_post_type_home');

function tu_reg_post_type_home() {

    //Change this when creating post type
    $post_type_name = __('Trang chủ', TEXT_DOMAIN);
    $post_type_name_lower = mb_strtolower($post_type_name, 'utf-8');
    $post_type_name_slug = tu_remove_accent($post_type_name, '-');
    $post_type_menu_position = 5;

    $labels = array(
        'name' => $post_type_name,
        'singular_name' => $post_type_name,
        'menu_name' => $post_type_name,
        'all_items' => __('Tất cả', TEXT_DOMAIN).' '.$post_type_name_lower,
        'add_new' => __('Thêm mới', TEXT_DOMAIN),
        'add_new_item' => __('Thêm mới', TEXT_DOMAIN).' '.$post_type_name_lower,
        'edit_item' => __('Chỉnh sửa', TEXT_DOMAIN).' '.$post_type_name_lower,
        'new_item' => $post_type_name,
        'view_item' => __('Xem chi tiết', TEXT_DOMAIN),
        'search_items' => __('Tìm kiếm', TEXT_DOMAIN),
        'not_found' => __('Không tìm thấy bản ghi nào', TEXT_DOMAIN),
        'not_found_in_trash' => __('Không có bản ghi nào trong thùng rác', TEXT_DOMAIN),
        'view' => __('Xem', TEXT_DOMAIN).' '.$post_type_name_lower,
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'show_in_nav_menus' => false,
        'show_ui' => true,

        //Change this when creating post type
        'description' => $post_type_name,
        'menu_position' => $post_type_menu_position,
        'menu_icon' => 'dashicons-admin-home',
        'supports' => array('title'),
        'rewrite' => array(
            'slug' => $post_type_name_slug
        ),

        //Use `Page Template` instead, it is more easy to custom
        'has_archive' => false
    );

    register_post_type('home', $args);

}

function tu_get_home_with_pagination($page = 1, $post_per_page = 10, $custom_args = array()) {

    $args = array(
        'post_type' => 'home',
        'posts_per_page' => $post_per_page,
        'paged' => $page,
        'post_status' => 'publish',
        'tax_query' => array()
    );

    $args = array_merge($args, $custom_args);

    $posts = new WP_Query($args);

    return $posts;
}


/**
 * POST META BOXES ----------- ----------- -----------
 */

add_action('admin_init', 'tu_add_meta_box_home');
function tu_add_meta_box_home() {

    function tu_display_meta_box_home_screen_2($post) {
        $post_id = $post->ID;
        $screen_2_label = get_post_meta($post_id, 'screen_2_label', true);
        $screen_2_desc = get_post_meta($post_id, 'screen_2_desc', true);
        $screen_2_image = get_post_meta($post_id, 'screen_2_image', true);
        ?>
        <table class="form-table">
            <input type="hidden" name="nonce" value="<?php echo wp_create_nonce('save_meta_box_home'); ?>">
            <tbody>
                <tr>
                    <th scope="row"><label for="screen_2_label">Tiêu đề</label></th>
                    <td>
                        <textarea style="width: 100%;" name="screen_2_label" id="screen_2_label" cols="53" rows="1"><?php echo $screen_2_label;  ?></textarea>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><label for="screen_2_desc">Mô tả ngắn</label></th>
                    <td>
                        <?php wp_editor($screen_2_desc, 'screen_2_desc', array('wpautop' => true, 'textarea_name' => 'screen_2_desc', 'textarea_rows' => 5, 'media_buttons' => false)); ?>
                    </td>
                </tr>
                <tr>
                    <?php tu_render_image_array_by_post_id_and_name('Ảnh mô tả', $post_id, 'screen_2_image'); ?>
                </tr>
            </tbody>
        </table>
        <?php
    }
    add_meta_box('tu_display_meta_box_home_screen_2', 'Frame 2 - Không gian sống trong mơ', 'tu_display_meta_box_home_screen_2', 'home', 'normal', 'high');

    function tu_display_meta_box_home_screen_3($post) {
        $post_id = $post->ID;
        $screen_3_label = get_post_meta($post_id, 'screen_3_label', true);
        $screen_3_desc = get_post_meta($post_id, 'screen_3_desc', true);
        $screen_3_image = get_post_meta($post_id, 'screen_3_image', true);
        ?>
        <table class="form-table">
            <input type="hidden" name="nonce" value="<?php echo wp_create_nonce('save_meta_box_home'); ?>">
            <tbody>
                <tr>
                    <th scope="row"><label for="screen_3_label">Tiêu đề</label></th>
                    <td>
                        <textarea style="width: 100%;" name="screen_3_label" id="screen_3_label" cols="53" rows="1"><?php echo $screen_3_label;  ?></textarea>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><label for="screen_3_desc">Mô tả ngắn</label></th>
                    <td>
                        <?php wp_editor($screen_3_desc, 'screen_3_desc', array('wpautop' => true, 'textarea_name' => 'screen_3_desc', 'textarea_rows' => 5, 'media_buttons' => false)); ?>
                    </td>
                </tr>
                <tr>
                    <?php tu_render_image_array_by_post_id_and_name('Ảnh mô tả', $post_id, 'screen_3_image'); ?>
                </tr>
            </tbody>
        </table>
        <?php
    }
    add_meta_box('tu_display_meta_box_home_screen_3', 'Frame 3 - Ốc đảo tươi xanh giữa thị thành', 'tu_display_meta_box_home_screen_3', 'home', 'normal', 'high');

    function tu_display_meta_box_home_screen_4($post) {
        $post_id = $post->ID;
        $screen_4_label = get_post_meta($post_id, 'screen_4_label', true);
        $screen_4_desc = get_post_meta($post_id, 'screen_4_desc', true);
        $screen_4_image = get_post_meta($post_id, 'screen_4_image', true);
        ?>
        <table class="form-table">
            <input type="hidden" name="nonce" value="<?php echo wp_create_nonce('save_meta_box_home'); ?>">
            <tbody>
                <tr>
                    <th scope="row"><label for="screen_4_label">Tiêu đề</label></th>
                    <td>
                        <textarea style="width: 100%;" name="screen_4_label" id="screen_4_label" cols="53" rows="1"><?php echo $screen_4_label;  ?></textarea>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><label for="screen_4_desc">Mô tả ngắn</label></th>
                    <td>
                        <?php wp_editor($screen_4_desc, 'screen_4_desc', array('wpautop' => true, 'textarea_name' => 'screen_4_desc', 'textarea_rows' => 5, 'media_buttons' => false)); ?>
                    </td>
                </tr>
                <tr>
                    <?php tu_render_image_array_by_post_id_and_name('Ảnh mô tả', $post_id, 'screen_4_image'); ?>
                </tr>
            </tbody>
        </table>
        <?php
    }
    add_meta_box('tu_display_meta_box_home_screen_4', 'Frame 4 - Tiện ích thượng đẳng cho cuộc sống tiện nghi', 'tu_display_meta_box_home_screen_4', 'home', 'normal', 'high');

    function tu_display_meta_box_home_screen_5($post) {
        $post_id = $post->ID;
        $screen_5_label = get_post_meta($post_id, 'screen_5_label', true);
        $screen_5_desc = get_post_meta($post_id, 'screen_5_desc', true);
        $screen_5_link = get_post_meta($post_id, 'screen_5_link', true);
        $screen_5_image = get_post_meta($post_id, 'screen_5_image', true);
        ?>
        <table class="form-table">
            <input type="hidden" name="nonce" value="<?php echo wp_create_nonce('save_meta_box_home'); ?>">
            <tbody>
                <tr>
                    <th scope="row"><label for="screen_5_label">Tiêu đề</label></th>
                    <td>
                        <textarea style="width: 100%;" name="screen_5_label" id="screen_5_label" cols="53" rows="1"><?php echo $screen_5_label;  ?></textarea>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><label for="screen_5_desc">Mô tả ngắn</label></th>
                    <td>
                        <?php wp_editor($screen_5_desc, 'screen_5_desc', array('wpautop' => true, 'textarea_name' => 'screen_5_desc', 'textarea_rows' => 5, 'media_buttons' => false)); ?>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><label for="screen_5_link">Link xem thêm</label></th>
                    <td>
                        <textarea style="width: 100%;" name="screen_5_link" id="screen_5_link" cols="53" rows="1"><?php echo $screen_5_link;  ?></textarea>
                    </td>
                </tr>
                <tr>
                    <?php tu_render_image_array_by_post_id_and_name('Slide ảnh mô tả', $post_id, 'screen_5_image'); ?>
                </tr>
            </tbody>
        </table>
        <?php
    }
    add_meta_box('tu_display_meta_box_home_screen_5', 'Frame 5 - Giá trị vượt trội', 'tu_display_meta_box_home_screen_5', 'home', 'normal', 'high');

    function tu_display_meta_box_home_screen_6($post) {
        $post_id = $post->ID;
        $screen_6_label = get_post_meta($post_id, 'screen_6_label', true);
        $screen_6_desc = get_post_meta($post_id, 'screen_6_desc', true);
        $screen_6_image = get_post_meta($post_id, 'screen_6_image', true);
        ?>
        <table class="form-table">
            <input type="hidden" name="nonce" value="<?php echo wp_create_nonce('save_meta_box_home'); ?>">
            <tbody>
                <tr>
                    <th scope="row"><label for="screen_6_label">Tiêu đề</label></th>
                    <td>
                        <textarea style="width: 100%;" name="screen_6_label" id="screen_6_label" cols="53" rows="1"><?php echo $screen_6_label;  ?></textarea>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><label for="screen_6_desc">Mô tả ngắn</label></th>
                    <td>
                        <?php wp_editor($screen_6_desc, 'screen_6_desc', array('wpautop' => true, 'textarea_name' => 'screen_6_desc', 'textarea_rows' => 5, 'media_buttons' => false)); ?>
                    </td>
                </tr>
                <tr>
                    <?php tu_render_image_array_by_post_id_and_name('Ảnh mô tả', $post_id, 'screen_6_image'); ?>
                </tr>
            </tbody>
        </table>
        <?php
    }
    add_meta_box('tu_display_meta_box_home_screen_6', 'Frame 6 - Vị trí trung tâm trong lòng trung tâm', 'tu_display_meta_box_home_screen_6', 'home', 'normal', 'high');

    function tu_display_meta_box_home_screen_7($post) {
        $post_id = $post->ID;
        $screen_7_image = get_post_meta($post_id, 'screen_7_image', true);
        $screen_7_link = get_post_meta($post_id, 'screen_7_link', true);
        ?>

        <?php tu_render_image_array_by_post_id_and_name('Ảnh thư viện nổi bật', $post_id, 'screen_7_image'); ?>
        <table class="form-table">
            <input type="hidden" name="nonce" value="<?php echo wp_create_nonce('save_meta_box_home'); ?>">
            <tbody>
                <tr>
                    <th scope="row"><label for="screen_7_link">Link xem thêm</label></th>
                    <td>
                        <textarea style="width: 100%;" name="screen_7_link" id="screen_7_link" cols="53" rows="1"><?php echo $screen_7_link;  ?></textarea>
                    </td>
                </tr>
            </tbody>
        </table>
        <?php
    }
    add_meta_box('tu_display_meta_box_home_screen_7', 'Frame 7 - Thư viện', 'tu_display_meta_box_home_screen_7', 'home', 'normal', 'high');

    function tu_display_meta_box_home_screen_8($post) {
        $post_id = $post->ID;
        $screen_8_label = get_post_meta($post_id, 'screen_8_label', true);
        $screen_8_desc = get_post_meta($post_id, 'screen_8_desc', true);
        $screen_8_hotline = get_post_meta($post_id, 'screen_8_hotline', true);
        $screen_8_note = get_post_meta($post_id, 'screen_8_note', true);
        ?>
        <table class="form-table">
            <input type="hidden" name="nonce" value="<?php echo wp_create_nonce('save_meta_box_home'); ?>">
            <tbody>
                <tr>
                    <th scope="row"><label for="screen_8_label">Tiêu đề</label></th>
                    <td>
                        <textarea style="width: 100%;" name="screen_8_label" id="screen_8_label" cols="53" rows="1"><?php echo $screen_8_label;  ?></textarea>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><label for="screen_8_desc">Mô tả ngắn</label></th>
                    <td>
                        <?php wp_editor($screen_8_desc, 'screen_8_desc', array('wpautop' => true, 'textarea_name' => 'screen_8_desc', 'textarea_rows' => 5, 'media_buttons' => false)); ?>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><label for="screen_8_hotline">Hotline đại lý</label></th>
                    <td>
                        <textarea style="width: 100%;" name="screen_8_hotline" id="screen_8_hotline" cols="53" rows="1"><?php echo $screen_8_hotline;  ?></textarea>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><label for="screen_8_note">Chú ý</label></th>
                    <td>
                        <?php wp_editor($screen_8_note, 'screen_8_note', array('wpautop' => true, 'textarea_name' => 'screen_8_note', 'textarea_rows' => 5, 'media_buttons' => false)); ?>
                    </td>
                </tr>
            </tbody>
        </table>
        <?php
    }
    add_meta_box('tu_display_meta_box_home_screen_8', 'Frame 8 - Đại lý phân phối', 'tu_display_meta_box_home_screen_8', 'home', 'normal', 'high');

}

add_action('save_post', 'tu_save_meta_home');
function tu_save_meta_home($post_id) {
    /** Save post meta box screen 2 */
    if ( isset( $_POST['screen_2_label'] ) ) {
        update_post_meta($post_id, 'screen_2_label', $_POST['screen_2_label']);
    }
    if ( isset( $_POST['screen_2_desc'] ) ) {
        update_post_meta($post_id, 'screen_2_desc', $_POST['screen_2_desc']);
    }
    if ( isset( $_POST['screen_2_image'] ) ) {
        update_post_meta($post_id, 'screen_2_image', $_POST['screen_2_image']);
    }

    /** Save post meta box screen 3 */
    if ( isset( $_POST['screen_3_label'] ) ) {
        update_post_meta($post_id, 'screen_3_label', $_POST['screen_3_label']);
    }
    if ( isset( $_POST['screen_3_desc'] ) ) {
        update_post_meta($post_id, 'screen_3_desc', $_POST['screen_3_desc']);
    }
    if ( isset( $_POST['screen_3_image'] ) ) {
        update_post_meta($post_id, 'screen_3_image', $_POST['screen_3_image']);
    }

    /** Save post meta box screen 4 */
    if ( isset( $_POST['screen_4_label'] ) ) {
        update_post_meta($post_id, 'screen_4_label', $_POST['screen_4_label']);
    }
    if ( isset( $_POST['screen_4_desc'] ) ) {
        update_post_meta($post_id, 'screen_4_desc', $_POST['screen_4_desc']);
    }
    if ( isset( $_POST['screen_4_image'] ) ) {
        update_post_meta($post_id, 'screen_4_image', $_POST['screen_4_image']);
    }

    /** Save post meta box screen 5 */
    if ( isset( $_POST['screen_5_label'] ) ) {
        update_post_meta($post_id, 'screen_5_label', $_POST['screen_5_label']);
    }
    if ( isset( $_POST['screen_5_desc'] ) ) {
        update_post_meta($post_id, 'screen_5_desc', $_POST['screen_5_desc']);
    }
    if ( isset( $_POST['screen_5_image'] ) ) {
        update_post_meta($post_id, 'screen_5_image', $_POST['screen_5_image']);
    }
    if ( isset( $_POST['screen_5_link'] ) ) {
        update_post_meta($post_id, 'screen_5_link', $_POST['screen_5_link']);
    }

    /** Save post meta box screen 6 */
    if ( isset( $_POST['screen_6_label'] ) ) {
        update_post_meta($post_id, 'screen_6_label', $_POST['screen_6_label']);
    }
    if ( isset( $_POST['screen_6_desc'] ) ) {
        update_post_meta($post_id, 'screen_6_desc', $_POST['screen_6_desc']);
    }
    if ( isset( $_POST['screen_6_image'] ) ) {
        update_post_meta($post_id, 'screen_6_image', $_POST['screen_6_image']);
    }

    /** Save post meta box screen 7 */
    if ( isset( $_POST['screen_7_image'] ) ) {
        update_post_meta($post_id, 'screen_7_image', $_POST['screen_7_image']);
    }
    if ( isset( $_POST['screen_7_link'] ) ) {
        update_post_meta($post_id, 'screen_7_link', $_POST['screen_7_link']);
    }

    /** Save post meta box screen 8 */
    if ( isset( $_POST['screen_8_label'] ) ) {
        update_post_meta($post_id, 'screen_8_label', $_POST['screen_8_label']);
    }
    if ( isset( $_POST['screen_8_desc'] ) ) {
        update_post_meta($post_id, 'screen_8_desc', $_POST['screen_8_desc']);
    }
    if ( isset( $_POST['screen_8_hotline'] ) ) {
        update_post_meta($post_id, 'screen_8_hotline', $_POST['screen_8_hotline']);
    }
    if ( isset( $_POST['screen_8_note'] ) ) {
        update_post_meta($post_id, 'screen_8_note', $_POST['screen_8_note']);
    }

}
