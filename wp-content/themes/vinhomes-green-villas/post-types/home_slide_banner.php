<?php
/**
 * INITIALIZE ----------- ----------- -----------
 */

add_action('init', 'tu_reg_post_type_slide_banner');

function tu_reg_post_type_slide_banner() {

    //Change this when creating post type
    $post_type_name = 'Trang chủ - Slide banner';
    $post_type_name_lower = mb_strtolower($post_type_name, 'utf-8');
    $post_type_name_slug = tu_remove_accent($post_type_name, '-');
    $post_type_menu_position = 6;

    $labels = array(
        'name' => $post_type_name,
        'singular_name' => $post_type_name,
        'menu_name' => $post_type_name,
        'all_items' => 'Tất cả '.$post_type_name_lower,
        'add_new' => 'Thêm mới ',
        'add_new_item' => 'Thêm mới '.$post_type_name_lower,
        'edit_item' => 'Chỉnh sửa '.$post_type_name_lower,
        'new_item' => $post_type_name,
        'view_item' => 'Xem chi tiết ',
        'search_items' => 'Tìm kiếm',
        'not_found' => 'Không tìm thấy bản ghi nào',
        'not_found_in_trash' => 'Không có bản ghi nào trong thùng rác',
        'view' => 'Xem'.$post_type_name_lower,
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'show_in_nav_menus' => false,
        'show_ui' => true,

        //Change this when creating post type
        'description' => $post_type_name,
        'menu_position' => $post_type_menu_position,
        'menu_icon' => 'dashicons-format-gallery',
        'supports' => array('title', 'thumbnail'),
        'rewrite' => array(
            'slug' => $post_type_name_slug
        ),
        'has_archive' => false
    );

    register_post_type('slide_banner', $args);
}

/**
 * RETRIEVING FUNCTIONS ----------- ----------- -----------
 */

/**
 * Get slide_banners
 *
 * @param int   $page
 * @param int   $post_per_page
 * @param array $custom_args
 *
 * @return WP_Query
 */
function tu_get_slide_banner_with_pagination($page = 1, $post_per_page = 10) {

    $args = array(
        'post_type' => 'slide_banner',
        'posts_per_page' => $post_per_page,
        'paged' => $page,
        'order' => 'asc',
        'orderby' => 'ID',
        'post_status' => 'publish',
    );

    $posts = new WP_Query($args);

    return $posts;
}


/**
 * POST META BOXES ----------- ----------- -----------
 */

add_action('admin_init', 'tu_add_post_meta_slide_banner');
function tu_add_post_meta_slide_banner() {
    function tu_display_post_meta_slide_banner($post) {
        $post_id = $post->ID;

        echo '<input type="hidden" name="nonce" value="' . wp_create_nonce('save_meta_box_slide_banner') . '">';

        $banner_title1 = get_post_meta($post_id, 'banner_title1', true);
        $banner_title2 = get_post_meta($post_id, 'banner_title2', true);

        $metaboxDataHome = array (
            array (
                'label' => __('Tiêu đề 1', TEXT_DOMAIN),
                'name' => 'banner_title1',
                'value' => $banner_title1,
                'type' => 'input',
            ),
            array (
                'label' => __('Tiêu đề 2', TEXT_DOMAIN),
                'name' => 'banner_title2',
                'value' => $banner_title2,
                'type' => 'input',
            ),

        );
        // Print Metabox
        metaboxPrint($metaboxDataHome);
        ?>

        <?php
    }

    add_meta_box (
        'tu_display_post_meta_slide_banner', __('Thông tin slide', TEXT_DOMAIN), 'tu_display_post_meta_slide_banner', 'slide_banner', 'normal', 'high'
    );
}
add_action('save_post', 'tu_save_post_meta_slide_banner');
function tu_save_post_meta_slide_banner($post_id) {
    if (get_post_type() == 'slide_banner' && isset($_POST['nonce']) && wp_verify_nonce($_POST['nonce'], 'save_meta_box_slide_banner')) {

        // Autosave, do nothing
        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
            return;
        // AJAX? Not used here
        if (defined('DOING_AJAX') && DOING_AJAX)
            return;
        // Check user permissions
        if (!current_user_can('edit_post', $post_id))
            return;
        // Return if it's a post revision
        if (false !== wp_is_post_revision($post_id))
            return;

        if ( isset($_POST['banner_title1']) ) {
            update_post_meta($post_id, 'banner_title1', sanitize_text_field($_POST['banner_title1']));
        }

        if ( isset($_POST['banner_title2']) ) {
            update_post_meta($post_id, 'banner_title2', sanitize_text_field($_POST['banner_title2']));
        }

    }
}

