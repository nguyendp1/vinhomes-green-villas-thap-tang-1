<?php
/**
 * INITIALIZE ----------- ----------- -----------
 */
add_action('init', 'tu_reg_post_type_introduce');
function tu_reg_post_type_introduce() {
    //Change this when creating post type
    $post_type_name = __('Giới thiệu', TEXT_DOMAIN);
    $post_type_name_lower = mb_strtolower($post_type_name, 'utf-8');
    $post_type_name_slug = tu_remove_accent($post_type_name, '-');
    $post_type_menu_position = 4;

    $labels = array(
        'name' => $post_type_name,
        'singular_name' => $post_type_name,
        'menu_name' => $post_type_name,
        'all_items' => __('Tất cả', TEXT_DOMAIN).' '.$post_type_name_lower,
        'add_new' => __('Thêm mới', TEXT_DOMAIN),
        'add_new_item' => __('Thêm mới', TEXT_DOMAIN).' '.$post_type_name_lower,
        'edit_item' => __('Chỉnh sửa', TEXT_DOMAIN).' '.$post_type_name_lower,
        'new_item' => $post_type_name,
        'view_item' => __('Xem chi tiết', TEXT_DOMAIN),
        'search_items' => __('Tìm kiếm', TEXT_DOMAIN),
        'not_found' => __('Không tìm thấy bản ghi nào', TEXT_DOMAIN),
        'not_found_in_trash' => __('Không có bản ghi nào trong thùng rác', TEXT_DOMAIN),
        'view' => __('Xem', TEXT_DOMAIN).' '.$post_type_name_lower,
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'show_in_nav_menus' => false,
        'show_ui' => true,

        //Change this when creating post type
        'description' => $post_type_name,
        'menu_position' => $post_type_menu_position,
        'menu_icon' => 'dashicons-welcome-widgets-menus',
        'supports' => array('title', '', '', ''),
        'rewrite' => array(
            'slug' => $post_type_name_slug
        ),

        //Use `Page Template` instead, it is more easy to custom
        'has_archive' => false
    );

    register_post_type('introduce', $args);
    
}

function tu_get_introduce_with_pagination($page = 1, $post_per_page = 10, $custom_args = array()) {

    $args = array(
        'post_type' => 'introduce',
        'posts_per_page' => $post_per_page,
        'paged' => $page,
        'post_status' => 'publish',
        'tax_query' => array(),
        'meta_query' => array()
    );

    $args = array_merge($args, $custom_args);

    $posts = new WP_Query($args);

    return $posts;
}

/**
 * POST META BOXES ----------- ----------- -----------
 */

add_action('admin_init', 'tu_add_meta_box_introduce');
function tu_add_meta_box_introduce() {
    function tu_display_meta_box_introduce_banner($post) {
        $post_id = $post->ID;
        $banner_title_1 = get_post_meta($post_id, 'banner_title_1', true);
        $banner_title_2 = get_post_meta($post_id, 'banner_title_2', true);
        ?>
        <table class="form-table">
            <input type="hidden" name="nonce" value="<?php echo wp_create_nonce('save_meta_box_introduce'); ?>">
            <tbody>
                <tr>
                    <th scope="row"><label for="banner_title_1">Tiêu đề 1</label></th>
                    <td>
                        <textarea style="width: 100%;" name="banner_title_1" id="banner_title_1" cols="53" rows="1"><?php echo $banner_title_1;  ?></textarea>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><label for="banner_title_2">Tiêu đề 2</label></th>
                    <td>
                        <textarea style="width: 100%;" name="banner_title_2" id="banner_title_2" cols="53" rows="1"><?php echo $banner_title_2;  ?></textarea>
                    </td>
                </tr>
            </tbody>
        </table>
        <?php
    }
    add_meta_box('tu_display_meta_box_introduce_banner', 'Frame 1 - Banner', 'tu_display_meta_box_introduce_banner', 'introduce', 'normal', 'high');

    function tu_display_meta_box_introduce_screen_2($post) {
        $post_id = $post->ID;
        $screen_2_title = get_post_meta($post_id, 'screen_2_title', true);
        $screen_2_desc = get_post_meta($post_id, 'screen_2_desc', true);
        ?>
        <table class="form-table">
            <input type="hidden" name="nonce" value="<?php echo wp_create_nonce('save_meta_box_introduce'); ?>">
            <tbody>
                <tr>
                    <th scope="row"><label for="screen_2_title">Tiêu đề</label></th>
                    <td>
                        <textarea style="width: 100%;" name="screen_2_title" id="screen_2_title" cols="53" rows="1"><?php echo $screen_2_title;  ?></textarea>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><label for="screen_2_desc">Mô tả ngắn</label></th>
                    <td>
                        <?php wp_editor($screen_2_desc, 'screen_2_desc', array('wpautop' => true, 'textarea_name' => 'screen_2_desc', 'textarea_rows' => 5, 'media_buttons' => false)); ?>
                    </td>
                </tr>
                <tr>
                    <?php tu_render_image_array_by_post_id_and_name('Ảnh mô tả', $post_id, 'screen_2_image'); ?>
                </tr>
            </tbody>
        </table>
        <?php
    }
    add_meta_box('tu_display_meta_box_introduce_screen_2', 'Frame 2', 'tu_display_meta_box_introduce_screen_2', 'introduce', 'normal', 'high');

    function tu_display_meta_box_introduce_screen_3($post) {
        $post_id = $post->ID;
        $screen_3_title = get_post_meta($post_id, 'screen_3_title', true);
        $screen_3_desc = get_post_meta($post_id, 'screen_3_desc', true);
        ?>
        <table class="form-table">
            <input type="hidden" name="nonce" value="<?php echo wp_create_nonce('save_meta_box_introduce'); ?>">
            <tbody>
                <tr>
                    <th scope="row"><label for="screen_3_title">Tiêu đề</label></th>
                    <td>
                        <textarea style="width: 100%;" name="screen_3_title" id="screen_3_title" cols="53" rows="1"><?php echo $screen_3_title;  ?></textarea>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><label for="screen_3_desc">Mô tả ngắn</label></th>
                    <td>
                        <?php wp_editor($screen_3_desc, 'screen_3_desc', array('wpautop' => true, 'textarea_name' => 'screen_3_desc', 'textarea_rows' => 5, 'media_buttons' => false)); ?>
                    </td>
                </tr>
                <tr>
                    <?php tu_render_image_array_by_post_id_and_name('Ảnh mô tả', $post_id, 'screen_3_image'); ?>
                </tr>
            </tbody>
        </table>
        <?php
    }
    add_meta_box('tu_display_meta_box_introduce_screen_3', 'Frame 3', 'tu_display_meta_box_introduce_screen_3', 'introduce', 'normal', 'high');

    function tu_display_meta_box_introduce_screen_4($post) {
        $post_id = $post->ID;
        $screen_4_title = get_post_meta($post_id, 'screen_4_title', true);
        $screen_4_desc = get_post_meta($post_id, 'screen_4_desc', true);
        ?>
        <table class="form-table">
            <input type="hidden" name="nonce" value="<?php echo wp_create_nonce('save_meta_box_introduce'); ?>">
            <tbody>
                <tr>
                    <th scope="row"><label for="screen_4_title">Tiêu đề</label></th>
                    <td>
                        <textarea style="width: 100%;" name="screen_4_title" id="screen_4_title" cols="53" rows="1"><?php echo $screen_4_title;  ?></textarea>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><label for="screen_4_desc">Mô tả ngắn</label></th>
                    <td>
                        <?php wp_editor($screen_4_desc, 'screen_4_desc', array('wpautop' => true, 'textarea_name' => 'screen_4_desc', 'textarea_rows' => 5, 'media_buttons' => false)); ?>
                    </td>
                </tr>
                <tr>
                    <?php tu_render_image_array_by_post_id_and_name('Ảnh mô tả', $post_id, 'screen_4_image'); ?>
                </tr>
            </tbody>
        </table>
        <?php
    }
    add_meta_box('tu_display_meta_box_introduce_screen_4', 'Frame 4', 'tu_display_meta_box_introduce_screen_4', 'introduce', 'normal', 'high');

    function tu_display_meta_box_introduce_screen_5($post) {
        $post_id = $post->ID;
        $screen_5_title = get_post_meta($post_id, 'screen_5_title', true);
        $screen_5_desc = get_post_meta($post_id, 'screen_5_desc', true);
        ?>
        <table class="form-table">
            <input type="hidden" name="nonce" value="<?php echo wp_create_nonce('save_meta_box_introduce'); ?>">
            <tbody>
                <tr>
                    <th scope="row"><label for="screen_5_title">Tiêu đề</label></th>
                    <td>
                        <textarea style="width: 100%;" name="screen_5_title" id="screen_5_title" cols="53" rows="1"><?php echo $screen_5_title;  ?></textarea>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><label for="screen_5_desc">Mô tả ngắn</label></th>
                    <td>
                        <?php wp_editor($screen_5_desc, 'screen_5_desc', array('wpautop' => true, 'textarea_name' => 'screen_5_desc', 'textarea_rows' => 5, 'media_buttons' => false)); ?>
                    </td>
                </tr>
                <tr>
                    <?php tu_render_image_array_by_post_id_and_name('Ảnh mô tả', $post_id, 'screen_5_image'); ?>
                </tr>
            </tbody>
        </table>
        <?php
    }
    add_meta_box('tu_display_meta_box_introduce_screen_5', 'Frame 5', 'tu_display_meta_box_introduce_screen_5', 'introduce', 'normal', 'high');
}

add_action('save_post', 'tu_save_meta_introduce');
function tu_save_meta_introduce($post_id) {

    // banner:
    if ( isset( $_POST['banner_title_1'] ) ) {
        update_post_meta($post_id, 'banner_title_1', $_POST['banner_title_1']);
    }
    if ( isset( $_POST['banner_title_2'] ) ) {
        update_post_meta($post_id, 'banner_title_2', $_POST['banner_title_2']);
    }

    //screen 2:
    if ( isset( $_POST['screen_2_title'] ) ) {
        update_post_meta($post_id, 'screen_2_title', $_POST['screen_2_title']);
    }
    if ( isset( $_POST['screen_2_desc'] ) ) {
        update_post_meta($post_id, 'screen_2_desc', $_POST['screen_2_desc']);
    }
    if ( isset( $_POST['screen_2_image'] ) ) {
        update_post_meta($post_id, 'screen_2_image', $_POST['screen_2_image']);
    }

    //screen 3:
    if ( isset( $_POST['screen_3_title'] ) ) {
        update_post_meta($post_id, 'screen_3_title', $_POST['screen_3_title']);
    }
    if ( isset( $_POST['screen_3_desc'] ) ) {
        update_post_meta($post_id, 'screen_3_desc', $_POST['screen_3_desc']);
    }
    if ( isset( $_POST['screen_3_image'] ) ) {
        update_post_meta($post_id, 'screen_3_image', $_POST['screen_3_image']);
    }

    //screen 4:
    if ( isset( $_POST['screen_4_title'] ) ) {
        update_post_meta($post_id, 'screen_4_title', $_POST['screen_4_title']);
    }
    if ( isset( $_POST['screen_4_desc'] ) ) {
        update_post_meta($post_id, 'screen_4_desc', $_POST['screen_4_desc']);
    }
    if ( isset( $_POST['screen_4_image'] ) ) {
        update_post_meta($post_id, 'screen_4_image', $_POST['screen_4_image']);
    }

    //screen 5:
    if ( isset( $_POST['screen_5_title'] ) ) {
        update_post_meta($post_id, 'screen_5_title', $_POST['screen_5_title']);
    }
    if ( isset( $_POST['screen_5_desc'] ) ) {
        update_post_meta($post_id, 'screen_5_desc', $_POST['screen_5_desc']);
    }
    if ( isset( $_POST['screen_5_image'] ) ) {
        update_post_meta($post_id, 'screen_5_image', $_POST['screen_5_image']);
    }
}

