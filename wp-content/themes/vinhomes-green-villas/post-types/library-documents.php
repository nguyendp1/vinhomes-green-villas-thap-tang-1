<?php

add_action('init', 'reg_post_type_album_doucument');

function reg_post_type_album_doucument() {
    //Change this when creating post type
    $post_type_name = __('Tài liệu dự án', TEXT_DOMAIN);
    $post_type_name_lower = mb_strtolower($post_type_name, 'utf-8');
    $post_type_name_slug = tu_remove_accent($post_type_name, '-');
    $post_type_menu_position = 10;

    $labels = array(
        'name' => $post_type_name,
        'singular_name' => $post_type_name,
        'menu_name' => $post_type_name,
        'all_items' => __('Tất cả', TEXT_DOMAIN) . ' ' . $post_type_name_lower,
        'add_new' => __('Thêm mới', TEXT_DOMAIN),
        'add_new_item' => __('Thêm mới', TEXT_DOMAIN) . ' ' . $post_type_name_lower,
        'edit_item' => __('Chỉnh sửa', TEXT_DOMAIN) . ' ' . $post_type_name_lower,
        'new_item' => $post_type_name,
        'view_item' => __('Xem chi tiết', TEXT_DOMAIN),
        'search_items' => __('Tìm kiếm', TEXT_DOMAIN),
        'not_found' => __('Không tìm thấy bản ghi nào', TEXT_DOMAIN),
        'not_found_in_trash' => __('Không có bản ghi nào trong thùng rác', TEXT_DOMAIN),
        'view' => __('Xem', TEXT_DOMAIN) . ' ' . $post_type_name_lower,
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'show_in_nav_menus' => false,
        'show_ui' => true,
        //Change this when creating post type
        'description' => $post_type_name,
        'menu_position' => $post_type_menu_position,
        'menu_icon' => 'dashicons-welcome-widgets-menus',
        'supports' => array('title'),
        'rewrite' => array(
            'slug' => $post_type_name_slug
        ),
        //Use `Page Template` instead, it is more easy to custom
        'has_archive' => false
    );

    register_post_type('document', $args);

}

/**
 * BACKEND HANDLES
 */


/**
 * @param int   $page
 * @param int   $post_per_page
 * @param array $options
 *
 * @return WP_Query
 */
function tu_get_document_with_pagination($page = 1, $post_per_page = 10) {
    $args = array(
        'post_type' => 'document',
        'posts_per_page' => $post_per_page,
        'paged' => $page,
        'post_status' => 'publish',
    );

    $posts = new WP_Query($args);

    return $posts;

}

/**
 * ADDING META BOXES
 */
add_action('admin_init', 'tu_add_post_meta_document');
function tu_add_post_meta_document() {

    function tu_display_post_meta_document($post) {
        $post_id = $post->ID;

        ?>
        <?php tu_render_document_by_post_id_and_key($post_id, 'document_file_id'); ?>
        <?php
    }
    add_meta_box('tu_display_post_meta_document', 'Quản lý tài liệu', 'tu_display_post_meta_document', 'document', 'normal', 'high');

}

/**
 * Saving meta box information
 */
add_action('save_post', 'tu_save_post_meta_document');

function tu_save_post_meta_document($post_id) {

    if (isset($_POST['document_file_id'])) {
        update_post_meta($post_id, 'document_file_id', (int) sanitize_text_field($_POST['document_file_id']));
    }

}
