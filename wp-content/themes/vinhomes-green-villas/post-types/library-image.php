<?php

/**
 * INITIALIZE POST TYPE
 */
/**
 * Register post type
 */
add_action('init', 'reg_post_type_album_images');

function reg_post_type_album_images() {
    //Change this when creating post type
    $post_type_name = __('Thư viện ảnh', TEXT_DOMAIN);
    $post_type_name_lower = mb_strtolower($post_type_name, 'utf-8');
    $post_type_name_slug = tu_remove_accent($post_type_name, '-');
    $post_type_menu_position = 5;

    $labels = array(
        'name' => $post_type_name,
        'singular_name' => $post_type_name,
        'menu_name' => $post_type_name,
        'all_items' => __('Tất cả', TEXT_DOMAIN) . ' ' . $post_type_name_lower,
        'add_new' => __('Thêm mới', TEXT_DOMAIN),
        'add_new_item' => __('Thêm mới', TEXT_DOMAIN) . ' ' . $post_type_name_lower,
        'edit_item' => __('Chỉnh sửa', TEXT_DOMAIN) . ' ' . $post_type_name_lower,
        'new_item' => $post_type_name,
        'view_item' => __('Xem chi tiết', TEXT_DOMAIN),
        'search_items' => __('Tìm kiếm', TEXT_DOMAIN),
        'not_found' => __('Không tìm thấy bản ghi nào', TEXT_DOMAIN),
        'not_found_in_trash' => __('Không có bản ghi nào trong thùng rác', TEXT_DOMAIN),
        'view' => __('Xem', TEXT_DOMAIN) . ' ' . $post_type_name_lower,
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'show_in_nav_menus' => false,
        'show_ui' => true,
        //Change this when creating post type
        'description' => $post_type_name,
        'menu_position' => $post_type_menu_position,
        'menu_icon' => 'dashicons-format-gallery',
        'supports' => array('title', 'thumbnail'),
        'rewrite' => array(
            'slug' => $post_type_name_slug
        ),
        //Use `Page Template` instead, it is more easy to custom
        'has_archive' => false
    );

    register_post_type('album_images', $args);

}

/**
 * BACKEND HANDLES
 */


/**
 * @param int   $page
 * @param int   $post_per_page
 * @param array $options
 *
 * @return WP_Query
 */
function tu_get_album_image_with_pagination($page = 1, $post_per_page = 10, $custom_args = array()) {

    $args = array(
        'post_type' => 'album_images',
        'posts_per_page' => $post_per_page,
        'paged' => $page,
        'post_status' => 'publish',
        'tax_query' => array(),
        'meta_query' => array()
    );

 // Push article is hot
    if (isset($custom_args['album_images_show_home'])) {

        array_push($args['meta_query'], array(
            'key'     => 'album_images_show_home',
            'value'   => $custom_args['album_images_show_home'],
            'compare' => '=',
        ));

        unset($custom_args['album_images_show_home']);
    }

    $args = array_merge($args, $custom_args);

    $posts = new WP_Query($args);

    return $posts;
}




/**
 * ADDING META BOXES
 */
add_action('admin_init', 'tu_add_meta_box_album_images');
function tu_add_meta_box_album_images() {

    function tu_display_meta_box_album_images_general($post) {
        $post_id = $post->ID;
        $album_images_show_home = get_post_meta($post_id, 'album_images_show_home', true);
        ?>
        <?php tu_render_image_array_by_post_id_and_name('Album ảnh', $post_id, 'album_images_img'); ?>
        <table class="form-table">
            <input type="hidden" name="nonce" value="<?php echo wp_create_nonce('save_meta_box_album_images'); ?>">
            <tbody>
                <tr>
                    <th scope="row"><label for="album_images_show_home"><?php _e('Hiển thị ở trang chủ', TEXT_DOMAIN) ?></label></th>
                    <td>
                        <input type="hidden" name="do" value="post"/>
                        <input type="checkbox" id="album_images_show_home" name="album_images_show_home"
                        value="1" <?php if ($album_images_show_home == 'true') echo 'checked="checked"'; ?> />
                    </td>
                </tr>
            </tbody>
        </table>

        <?php
    }

    add_meta_box(
        'tu_display_meta_box_album_images_general', 'Thông tin cơ bản', 'tu_display_meta_box_album_images_general', 'album_images', 'normal', 'high'
    );
}

add_action('save_post', 'tu_save_meta_box_album_images');
function tu_save_meta_box_album_images($post_id) {
    if (get_post_type() == 'album_images' && isset($_POST['nonce']) && wp_verify_nonce($_POST['nonce'], 'save_meta_box_album_images')) {

        $album_images_show_home = isset($_POST['album_images_show_home']) && (int) $_POST['album_images_show_home'] ? 'true' : 'false';
        update_post_meta($post_id, 'album_images_show_home', $album_images_show_home);
    }
    if (isset($_POST['album_images_img'])) {
        update_post_meta($post_id, 'album_images_img', $_POST['album_images_img']);
    } else {
        delete_post_meta($post_id, 'album_images_img');
    }
}