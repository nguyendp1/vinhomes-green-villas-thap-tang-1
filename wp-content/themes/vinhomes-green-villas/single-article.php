<?php //  Template Name: Tin tức - chi tiêt ?>

<?php get_header(); 
$post_id = get_the_ID();
$page_thumbnail_url = has_post_thumbnail($post_id) ? tu_get_post_thumbnail_src_by_post_id($post_id,'article') : IMAGE_URL . "/chitiettintuc/maskGroup.png";
$current_term = tu_get_first_term_by_post_id($post_id, 'article_category');
$term_name = get_term( $current_term)->name;
$term_link = get_term_link( $current_term);
$tags_of_post = tu_get_all_term_by_post_id($post_id, 'article_tags');
?>
<div class="chitiettintuc">
	<div class="baner" style="background-image:url('<?php echo $page_thumbnail_url; ?>')">
	</div>
	<div class="colum">
		<div class="left">
			<?php while ( have_posts() ) : the_post(); ?>
				<?php
				$title = get_the_title($post_id);
				$day = get_the_date( 'd', $post_id );
				$month_year = get_the_date( 'm, Y', $post_id );
				?>
				<a href="<?php echo $term_link;?>"> <i class="fa fa-angle-left" aria-hidden="true"></i> Trở lại danh sách</a>
				<h1><?php echo $title; ?></h1>
				<div class="text">
					<p><?php echo $day; ?> tháng <?php echo $month_year; ?></p>
					<a href="javascript:void(0)"><?php echo $term_name;?></a>
				</div>
				<div class="content">
					<?php the_content(); ?>
					<div class="evaluate">
						<div class="stars">
							<i class="fa fa-star-o active" aria-hidden="true"></i>
							<i class="fa fa-star-o" aria-hidden="true"></i>
							<i class="fa fa-star-o" aria-hidden="true"></i>
							<i class="fa fa-star-o" aria-hidden="true"></i>
							<i class="fa fa-star-o" aria-hidden="true"></i>
						</div>
						<div class="text">
							<p>5.0</p>
							<a href="#">3 đánh giá</a>
						</div>
					</div>
				</div>
				<div class="bottom">
					<div class="hastag">
						<?php if ( isset( $tags_of_post ) && $tags_of_post ) : ?>
							<?php foreach ( $tags_of_post as $tag ) : ?>
								<?php
								$term_id = $tag->term_id;
								$tag_name = $tag->name;
								$tag_link = get_term_link($term_id);
								?>
								<a href="<?php echo $tag_link; ?>"><span>#</span><?php echo $tag_name; ?> </a>
							<?php endforeach; ?>
						<?php endif; ?>
					</div>
					<div class="social-chitiettintuc">
						<p> Chia sẻ :</p>
						<a href="https://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>" target="_blank">
							<div class="img" style="background-image:url('<?php echo IMAGE_URL.'/chitiettintuc/facebook.png'; ?>')"></div>
						</a>
						<a href="https://twitter.com/share?url=<?php the_permalink(); ?>" target="_blank">
							<div class="img" style="background-image:url('<?php echo IMAGE_URL.'/chitiettintuc/twitter.png'; ?>')"></div>
						</a>
						<a href="https://www.instagram.com/share?url=<?php the_permalink(); ?>" target="_blank">
							<div class="img" style="background-image:url('<?php echo IMAGE_URL.'/chitiettintuc/linkedin.png'; ?>')"></div>
						</a>
						<a href="https://www.youtube.com/share?url=<?php the_permalink(); ?>" target="_blank">
							<div class="img" style="background-image:url('<?php echo IMAGE_URL.'/chitiettintuc/youtube.png'; ?>')"></div>
						</a>
					</div>
				</div>
			<?php endwhile; ?>
		</div>
		<div class="right">
			<div class="news">
				<h3>Tin liên quan</h3>
				<?php
				$current_terms =  wp_get_post_terms($post_id, 'article_category', array('fields' => 'ids'));
				$article_relations = tu_get_article_with_pagination(1, 4, array('post__not_in' => array($post_id), 'article_category' => $current_terms));
				?>
				<?php if ( $article_relations->have_posts() ) : ?>
					<?php while ( $article_relations->have_posts() ) : $article_relations->the_post(); ?>
						<?php
						$post_id = get_the_ID();
						$title = get_the_title($post_id);
						$permalink = get_permalink($post_id);
						$day = get_the_date( 'd', $post_id );
						$month_year = get_the_date( 'm, Y', $post_id );
						$image = has_post_thumbnail( $post_id ) ? tu_get_post_thumbnail_src_by_post_id( $post_id, 'article' ) : '';
						?>
						<a href="<?php echo $permalink;?>" class="item">
							<div class="img"style="background-image:url('<?php echo $image;?>')"></div>
							<div class="text">
								<div class="title"><?php echo $title;?> </div>	
								<p><?php echo $day; ?>  <?php echo $month_year; ?></p>
							</div>
						</a>
					<?php endwhile; ?>
				<?php endif; ?>
				<a class='button' href="<?php echo $term_link;?>">Xem thêm</a>	
			</div>
		</div>
	</div>
</div>
<?php include_once (TEMPLATE_PATH. '/partials/home/section_8.php');?>
<?php get_footer(); ?>