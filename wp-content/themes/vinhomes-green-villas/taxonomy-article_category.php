<?php 
get_header(); 
$current_term_id = get_queried_object()->term_id;
$article_is_hot = tu_get_article_with_pagination(1, 3, array('article_category' => $current_term_id));
$current_term_name = get_queried_object()->name;
$terms_article = tu_get_terms_by_parent_id( 'article_category', 0);
?>
<div class="page-news">
	<div class="page-banner">
		<div class="breadcrumb">
			<i class="fa fa-home" aria-hidden="true"></i>
			<a href="<?php echo HOME_URL; ?>" class="">Trang chủ / </a>
			<a href="javascript:void(0)"class="active"> Tin tức</a>
		</div>
		<?php $array_not_in = array(); ?>
		<?php if ( $article_is_hot->have_posts() ) : ;?>
			<?php while ( $article_is_hot->have_posts() ) : $article_is_hot->the_post(); ?>
				<?php
				$post_id = get_the_ID();
				array_push($array_not_in, $post_id);
				$title = get_the_title($post_id);
				$day = get_the_date( 'd', $post_id );
				$month_year = get_the_date( 'm, Y', $post_id );
				$permalink = get_permalink($post_id);
				$thumbnail = has_post_thumbnail( $post_id ) ? tu_get_post_thumbnail_src_by_post_id( $post_id, 'article_is_hot' ) : '';
				?>
				<a href="<?php echo $permalink; ?>" class="thumnail" style="background-image: url('<?php echo $thumbnail; ?>');">
					<div class="desc">
						<div class="news-project"><?php echo $current_term_name; ?></div>
						<div class="title"><?php echo $title; ?></div>
						<div class="time"><?php echo $day; ?> tháng <?php echo $month_year; ?></div>
					</div> 
				</a>
			<?php endwhile; ?>
		<?php endif; ?>
	</div>
	<div class="content-main">
		<div class="nav">
			<?php foreach ( $terms_article as $term ) : ?>
				<?php
				$article_id = $term->term_id;
				$article_name = $term->name;
				$term_link = get_term_link($article_id);
				$active = $current_term_id == $article_id ? 'active' : '' ;
				?>
				<a href="<?php echo $term_link; ?>" class="<?php echo $active;?>"><?php echo $article_name; ?></a>
			<?php endforeach; ?>
		</div>
		<div class="list-news">
			<div class="content-list">
				<?php 
				$article = tu_get_article_with_pagination(1, -1, array('article_category' => $current_term_id, 'post__not_in' => $array_not_in));
				?>
				<?php if ( $article->have_posts() ) : ;?>
					<?php while ( $article->have_posts() ) : $article->the_post(); ?>
						<?php
						$post_id = get_the_ID();
						$title = get_the_title($post_id);
						$day = get_the_date( 'd', $post_id );
						$month_year = get_the_date( 'm, Y', $post_id );
						$permalink = get_permalink($post_id);
						$thumbnail = has_post_thumbnail( $post_id ) ? tu_get_post_thumbnail_src_by_post_id( $post_id, 'article_is_hot' ) : '';
						?>
						<a href="<?php echo $permalink;?>" class="thumnail-list">
							<div class="img" style="background-image: url('<?php echo $thumbnail;?>');"></div>
							<div class="desc-list">
								<div class="time"><?php echo $day; ?> tháng <?php echo $month_year; ?></div>
								<div class="txt"><?php echo $title;?></div>
							</div>
						</a>
					<?php endwhile; ?>
				<?php endif; ?>
			</div>
			<div class="link sunset-load-more">xem thêm</div>
		</div>
	</div>
	<?php include_once (TEMPLATE_PATH. '/partials/home/section_8.php');?>
</div>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(function(){
			$('.thumnail-list').slice(0, 4).show();
			$('.sunset-load-more').on('click',function(e){
				e.preventDefault();
				$('.thumnail-list:hidden').slice(0, 2).show();
			});
		});
	});
</script>
<?php get_footer(); ?>


